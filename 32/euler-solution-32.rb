# filename 'euler-solution-32.rb'
# By: Louis Casillas, oxaric@gmail.com

# Euler Problem #32
# Find the sum of all numbers that can be written as pandigital products.

product_array = Array.new
num_array = Array.new

for i in (1..2000)
	for j in (1..2000)
		temp = i * j
		temp_s = i.to_s + j.to_s + temp.to_s

		if (temp_s.size == 9)

			temp_s = temp_s.split(//).sort.join
			
			is_special_product = true

			for k in (0..8)
				if ( temp_s[k, 1] != (k + 1).to_s )
					is_special_product = false
				end
			end

			if is_special_product
				puts "Found one: " + i.to_s + " * " + j.to_s + " = " + temp.to_s
				product_array.insert( 0, temp )
			end	
		end
	end
end


product_array.uniq!
sum = 0

for i in (0...product_array.size)
	sum += product_array[i]	
	puts "sum: " + sum.to_s
end

puts "The sum is: " + sum.to_s
