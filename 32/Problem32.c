/*
V2Ugc2hhbGwgc2F5IHRoYXQgYW4gbi1kaWdpdCBudW1iZXIgaXMgcGFuZGlnaXRh
bCBpZiBpdCBtYWtlcyB1c2Ugb2YgYWxsIHRoZSBkaWdpdHMgMSB0byBuIGV4YWN0
bHkgb25jZTsgZm9yIGV4YW1wbGUsIHRoZSA1LWRpZ2l0IG51bWJlciwgMTUyMzQs
IGlzIDEgdGhyb3VnaCA1IHBhbmRpZ2l0YWwuDQoNClRoZSBwcm9kdWN0IDcyNTQg
aXMgdW51c3VhbCwgYXMgdGhlIGlkZW50aXR5LCAzOSDDlyAxODYgPSA3MjU0LCBj
b250YWluaW5nIG11bHRpcGxpY2FuZCwgbXVsdGlwbGllciwgYW5kIHByb2R1Y3Qg
aXMgMSB0aHJvdWdoIDkgcGFuZGlnaXRhbC4NCg0KRmluZCB0aGUgc3VtIG9mIGFs
bCBwcm9kdWN0cyB3aG9zZSBtdWx0aXBsaWNhbmQvbXVsdGlwbGllci9wcm9kdWN0
IGlkZW50aXR5IGNhbiBiZSB3cml0dGVuIGFzIGEgMSB0aHJvdWdoIDkgcGFuZGln
aXRhbC4NCg0KSElOVDogU29tZSBwcm9kdWN0cyBjYW4gYmUgb2J0YWluZWQgaW4g
bW9yZSB0aGFuIG9uZSB3YXkgc28gYmUgc3VyZSB0byBvbmx5IGluY2x1ZGUgaXQg
b25jZSBpbiB5b3VyIHN1bS4=
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include "../EulerLib/euler.h"
#include <inttypes.h>

uint64_t MakeNumber(unsigned int n,unsigned int m,unsigned int p)
{
  uint64_t ret = p;
  ret += (m * pow(10,HowManyDigits(ret)));
  ret += (n * pow(10,HowManyDigits(ret)));
  return ret;
}


int main()
{
  // clock_t start = clock();
  unsigned i,j;
  uint64_t n;
  uint64_t sum = 0;

  for (i = 1; i < 2000; i++)
  {
    for (j = i; j < 2000; j++)
    {
      n = MakeNumber(i,j,(i*j));
      if((HowManyDigits(n)== 9)&&(isPanDigital(n)))
      {
        //printf("Found one :%u x %u = %u\n",i,j,(i*j));
        if(PreviouslyEncountered(i*j) == 0)
        {
          sum += (i*j);
        }
        //printf("%u x %u = %u Pan Digital Product %"PRIu64" current sum %"PRIu64"\n",i,j,(i*j),n,sum);
      }
    }
  }
  printf("%"PRIu64"\n",sum);
  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}
