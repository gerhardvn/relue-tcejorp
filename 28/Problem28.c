/*
U3RhcnRpbmcgd2l0aCB0aGUgbnVtYmVyIDEgYW5kIG1vdmluZyB0byB0aGUgcmln
aHQgaW4gYSBjbG9ja3dpc2UgZGlyZWN0aW9uIGEgNSBieSA1IHNwaXJhbCBpcyBm
b3JtZWQgYXMgZm9sbG93czoNCjQzIDQ0IDQ1IDQ2IDQ3IDQ4IDQ5DQo0MiAyMSAy
MiAyMyAyNCAyNSAyNg0KNDEgMjAgIDcgIDggIDkgMTAgMjcNCjQwIDE5ICA2ICAx
ICAyIDExIDI4DQozOSAxOCAgNSAgNCAgMyAxMiAyOQ0KMzggMTcgMTYgMTUgMTQg
MTMgMzANCjM3IDM2IDM1IDM0IDMzIDMyIDMxDQpJdCBjYW4gYmUgdmVyaWZpZWQg
dGhhdCB0aGUgc3VtIG9mIGJvdGggZGlhZ29uYWxzIGlzIDEwMS4NCldoYXQgaXMg
dGhlIHN1bSBvZiBib3RoIGRpYWdvbmFscyBpbiBhIDEwMDEgYnkgMTAwMSBzcGly
YWwgZm9ybWVkIGluIHRoZSBzYW1lIHdheT8NCg==*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define  SPIRAL_SIZE 1001  /* Must be odd*/

int main()
{
  // clock_t start = clock();

  unsigned count = 1;
  unsigned step = 2;
  unsigned turns = 0;
  unsigned sum = 1;

  for (count = 3; count <= (SPIRAL_SIZE*SPIRAL_SIZE); count += step)
  {
    //printf("%u ", count);
    sum += count;
    turns++;
    if(turns == 4)
    {
      turns = 0;
      step += 2;
    }
  }

  printf("%u\n", sum);

  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}