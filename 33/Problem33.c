/*
VGhlIGZyYWN0aW9uIDQ5Lzk4IGlzIGEgY3VyaW91cyBmcmFjdGlvbiwgYXMgYW4g
aW5leHBlcmllbmNlZCBtYXRoZW1hdGljaWFuIGluIGF0dGVtcHRpbmcgdG8gc2lt
cGxpZnkgaXQgbWF5IGluY29ycmVjdGx5IGJlbGlldmUgdGhhdCA0OS85OCA9IDQv
OCwgd2hpY2ggaXMgY29ycmVjdCwgaXMgb2J0YWluZWQgYnkgY2FuY2VsbGluZyB0
aGUgOXMuDQoNCldlIHNoYWxsIGNvbnNpZGVyIGZyYWN0aW9ucyBsaWtlLCAzMC81
MCA9IDMvNSwgdG8gYmUgdHJpdmlhbCBleGFtcGxlcy4NCg0KVGhlcmUgYXJlIGV4
YWN0bHkgZm91ciBub24tdHJpdmlhbCBleGFtcGxlcyBvZiB0aGlzIHR5cGUgb2Yg
ZnJhY3Rpb24sIGxlc3MgdGhhbiBvbmUgaW4gdmFsdWUsIGFuZCBjb250YWluaW5n
IHR3byBkaWdpdHMgaW4gdGhlIG51bWVyYXRvciBhbmQgbnVtZXJhdG9yLg0KDQpJ
ZiB0aGUgcHJvZHVjdCBvZiB0aGVzZSBmb3VyIGZyYWN0aW9ucyBpcyBnaXZlbiBp
biBpdHMgbG93ZXN0IGNvbW1vbiB0ZXJtcywgZmluZCB0aGUgdmFsdWUgb2YgdGhl
IGRlbm9taW5hdG9yLg==
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include "../EulerLib/euler.h"
#include <inttypes.h>

unsigned gcd(unsigned a, unsigned b)
{
  unsigned r;
  while ((r = a % b) != 0)
  {
    a = b;
    b = r;
  }
  return b;
}

unsigned remove_digit(int num, int n) 
{ 
  if(n == 1)
  {
    num /= 10;
  }
  else if(n == 2)
  {
    num %= 10;
  }
  
  return num; 
}

int main()
{
  // clock_t start = clock();
  
  unsigned answer_n = 1;
  unsigned answer_d = 1;

  for (unsigned denominator = 10; denominator < 100; denominator++)
  {
    unsigned denominator_digits = 2;

    for (unsigned numerator = 10; numerator < denominator; numerator++)
    {
      unsigned numerator_digits = 2;
      
      for (unsigned i = 1; i <= denominator_digits; i++)
      {
        unsigned denominator_digit = GetDigit(denominator, i);
        
        if (denominator_digit != 0)
        {
          for (unsigned j = 1; j <= numerator_digits; j++)
          {
            unsigned numerator_digit = GetDigit(numerator,j);
    
            if (numerator_digit != 0)
            {
            
              if(numerator_digit == denominator_digit)
              {
                unsigned x = remove_digit(numerator,j);
                unsigned z = remove_digit(denominator, i);

                if ((x != 0) && (z != 0))
                {
                  unsigned answer1 =  (unsigned)(((double)numerator / (double)denominator)*100000.0);
                  unsigned answer2 =  (unsigned)(((double)x/(double)z)*100000.0);
                  
                  if(answer1 == answer2)
                  {
                    //printf ("%u/%u    %u/%u\n",numerator,denominator,answer1,answer2);
                    answer_n *= numerator;
                    answer_d *= denominator;
                  }
                }
              }
            }
          }    
        }
      }
    }    
  }    

  //printf("%u/%u\n", answer_n, answer_d);
  unsigned gcd_answer = gcd(answer_n, answer_d);
  //printf("%u/%u\n", answer_n/gcd_answer, answer_d/gcd_answer);
  printf("%u\n",answer_d/gcd_answer);
  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}