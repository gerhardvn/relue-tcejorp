/*
SWYgd2UgbGlzdCBhbGwgdGhlIG5hdHVyYWwgbnVtYmVycyBiZWxvdyAxMCB0aGF0
IGFyZSBtdWx0aXBsZXMgb2YgMyBvciA1LA0Kd2UgZ2V0IDMsIDUsIDYgYW5kIDku
IFRoZSBzdW0gb2YgdGhlc2UgbXVsdGlwbGVzIGlzIDIzLg0KDQpGaW5kIHRoZSBz
dW0gb2YgYWxsIHRoZSBtdWx0aXBsZXMgb2YgMyBvciA1IGJlbG93IDEwMDAu
*/

#include <stdio.h>
#include <stdlib.h>

int DividableSum(int target, int div)
{
  int p = target / div;
  return ((div * (p * (p + 1))) / 2);
}

int main()
{
  int sum = 0;

  sum = DividableSum(999,3) + DividableSum(999,5) - DividableSum(999,15);
  printf("%i\n",sum);
  return 0;
}
