/*
SXQgd2FzIHByb3Bvc2VkIGJ5IENocmlzdGlhbiBHb2xkYmFjaCB0aGF0IGV2ZXJ5
IG9kZCBjb21wb3NpdGUgbnVtYmVyIGNhbiBiZSB3cml0dGVuIGFzIHRoZSBzdW0g
b2YgYSBwcmltZSBhbmQgdHdpY2UgYSBzcXVhcmUuDQoNCjkgPSA3ICsgMsOXMV4o
MikNCjE1ID0gNyArIDLDlzJeKDIpDQoyMSA9IDMgKyAyw5czXigyKQ0KMjUgPSA3
ICsgMsOXM14oMikNCjI3ID0gMTkgKyAyw5cyXigyKQ0KMzMgPSAzMSArIDLDlzFe
KDIpDQoNCkl0IHR1cm5zIG91dCB0aGF0IHRoZSBjb25qZWN0dXJlIHdhcyBmYWxz
ZS4NCg0KV2hhdCBpcyB0aGUgc21hbGxlc3Qgb2RkIGNvbXBvc2l0ZSB0aGF0IGNh
bm5vdCBiZSB3cml0dGVuIGFzIHRoZSBzdW0gb2YgYSBwcmltZSBhbmQgdHdpY2Ug
YSBzcXVhcmU/
*/

#include <stdint.h>
#include "../EulerLib/euler.h"
#include <stdio.h>

int canDecompose(unsigned int n)
{
  unsigned i,j;

  for (i = 2; i < n; i = ToNextPrime(i))
  {
    for (j = 1; (j*j) < (n - i); j++)
    {
      if((i + (2 * j * j)) == n)
      {
        //printf("%u = %u + 2 x %u^2\n",n,i,j);
        return 1;
      }
    }
  }

  return 0;
}

int main()
{
  unsigned int i = 3;

  while (1)
  {
    if(isComposite(i))
    {
      if (canDecompose(i) == 0)
      {
        break;
      }
    }

    i += 2;
  }

  printf ("%u\n",i);

  return 0;
}
