/*
ICBJbnRlZ2VyIHJpZ2h0IHRyaWFuZ2xlcw0KICBJZiAgcCAgaXMgdGhlIHBlcmlt
ZXRlciBvZiBhIHJpZ2h0IGFuZ2xlIHRyaWFuZ2xlIHdpdGggaW50ZWdyYWwgbGVu
Z3RoIHNpZGVzLCB7ICBhICAsICBiICAsICBjICB9LCB0aGVyZSBhcmUgZXhhY3Rs
eSB0aHJlZSBzb2x1dGlvbnMgZm9yICBwICA9IDEyMC4NCiAgezIwLDQ4LDUyfSwg
ezI0LDQ1LDUxfSwgezMwLDQwLDUwfQ0KICBGb3Igd2hpY2ggdmFsdWUgb2YgIHAg
IOKJpCAxMDAwLCBpcyB0aGUgbnVtYmVyIG9mIHNvbHV0aW9ucyBtYXhpbWlzZWQ/
*/ 

#include <stdio.h>
#include <math.h>

/* mostly Bruteforce solution*/

int main()
{
  unsigned p, max_p;
  unsigned a,b,c;
  unsigned answer = 0, max = 0;

  for (p = 5; p <= 1000; p++)
  {
    //printf("p = %u -----------\n",p);
    answer = 0;
    
    for(c = 3; (c < (p/2)); c++)
    {
      for(b = 2; b < c; b++)
      {
        for(a = 1; a < b; a++)
        {
          if((a+b+c) > p)
          {
            break;
          }
          
          if((a+b+c) == p)
          {
            if(( pow(a,2) + pow(b,2)) == pow(c,2))
            {
              //printf("{%u,%u,%u}\n",a,b,c);
              answer++;
            }
          }
        }
      }
    }
    //printf("%u\n",answer);
    if (answer > max)
    {
      //printf("*\n");
      max = answer;
      max_p = p;
    }
  }

  printf("%u\n",max_p);
  
  return 0;
}
