/*
VGhlIGFyaXRobWV0aWMgc2VxdWVuY2UsIDE0ODcsIDQ4MTcsIDgxNDcsIGluIHdo
aWNoIGVhY2ggb2YgdGhlIHRlcm1zIGluY3JlYXNlcw0KYnkgMzMzMCwgaXMgdW51
c3VhbCBpbiB0d28gd2F5czogKGkpIGVhY2ggb2YgdGhlIHRocmVlIHRlcm1zIGFy
ZSBwcmltZSwgYW5kLA0KKGlpKSBlYWNoIG9mIHRoZSA0LWRpZ2l0IG51bWJlcnMg
YXJlIHBlcm11dGF0aW9ucyBvZiBvbmUgYW5vdGhlci4NCg0KVGhlcmUgYXJlIG5v
IGFyaXRobWV0aWMgc2VxdWVuY2VzIG1hZGUgdXAgb2YgdGhyZWUgMS0sIDItLCBv
ciAzLWRpZ2l0IHByaW1lcywNCmV4aGliaXRpbmcgdGhpcyBwcm9wZXJ0eSwgYnV0
IHRoZXJlIGlzIG9uZSBvdGhlciA0LWRpZ2l0IGluY3JlYXNpbmcgc2VxdWVuY2Uu
DQoNCldoYXQgMTItZGlnaXQgbnVtYmVyIGRvIHlvdSBmb3JtIGJ5IGNvbmNhdGVu
YXRpbmcgdGhlIHRocmVlIHRlcm1zIGluIHRoaXMgc2VxdWVuY2U/
*/

#include <stdint.h>
#include "../EulerLib/euler.h"
#include <stdio.h>
#include <string.h>

#include <stddef.h>

/* 
 * Computes the next lexicographical permutation of the specified
 * array of integers in place, returning a Boolean to indicate
 * whether a next permutation existed. (Returns false when the
 * argument is already the last possible permutation.)
 */
/* based on https://www.nayuki.io/page/next-lexicographical-permutation-algorithm */
/* alternative to c++ next_permutation*/
int next_permutation(int array[], size_t length) {
    // Find non-increasing suffix
    if (length == 0)
        return 0;
    size_t i = length - 1;
    while (i > 0 && array[i - 1] >= array[i])
        i--;
    if (i == 0)
        return 0;
    
    // Find successor to pivot
    size_t j = length - 1;
    while (array[j] <= array[i - 1])
        j--;
    int temp = array[i - 1];
    array[i - 1] = array[j];
    array[j] = temp;
    
    // Reverse suffix
    j = length - 1;
    while (i < j) {
        temp = array[i];
        array[i] = array[j];
        array[j] = temp;
        i++;
        j--;
    }
    return 1;
}

void NumberToArray(unsigned int n, int a[32])
{
  unsigned int d = HowManyDigits(n);
  int i;

  for (i = (d - 1); i >= 0; i--)
  {
    a[i] = n % 10;
    n /= 10;
  }
}

unsigned int ArrayToNumber(int a[32], unsigned int d)
{
  unsigned int n = 0;
  unsigned int i;

  for (i = 0; i < d; i++)
  {
    n *= 10;
    n += a[i];
  }
  return n;
}

unsigned int processPerms(int d, uint32_t p[10])
{
  int i,j,k;
  uint32_t div;

  if(d < 3)
  {
    return 0;
  }

  if(d > 3)
  {
    for (k = 0; k < (d - 2); k++)
    {
      for (i = k + 1; i < (d - 1); i++)
      {
        div = p[i] - p[k];
        for (j = i + 1; j < d - 1; j++)
        {
          if ((p[j+i] - p[i]) == div)
          {
            //printf("%u%u%u\n", p[k], p[i], p[j]);
          }
        }
      }
    }
  }
  else if((p[2] - p[1]) == (p[1] - p[0]))
  {
    for (i = 0; i < d; i++)
    {
      printf("%u",p[i]);
    }
    printf("\n");
  }
  return 0;
}


int allUniqueDigits(uint64_t n)
{
  int digits = HowManyDigits(n);
  int occured[10];
  int i;
  int digit;

  memset((void *)&occured,0,40);

  for(i = 0; i < digits; i++)
  {
    digit = GetDigit(n,i+1);

    if(occured[digit])
    {
      return 0;
    }
    else
    {
      occured[digit] = 1;
    }
  }

  return 1;
}

int main()
{
  unsigned int i;
  int a[4];
  unsigned int perm;
  uint32_t found[20] = {0};
  int foundi = 0;

  for (i = 1001; i < 9999; i += 2)
  {
    if(isPrime(i))
    {
      NumberToArray(i,a);

      found[foundi] = i;
      foundi++;

      while(next_permutation(a,4))
      {
        perm = ArrayToNumber(a, 4);
        if((perm > i) && isPrime(perm))
        {
          found[foundi] = perm;
          foundi++;
        }
      }

      processPerms(foundi, found);
      foundi = 0;
    }
  }

  return 0;
}

