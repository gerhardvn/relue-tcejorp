/*
Q29uc2lkZXIgYWxsIGludGVnZXIgY29tYmluYXRpb25zIG9mIGFeKGIpIGZvciAy
IDw9IGEgPD0gNSBhbmQgMiA8PSBiIDw9IDU6DQoNCiAgICAyXigyKT00LCAyXigz
KT04LCAyXig0KT0xNiwgMl4oNSk9MzINCiAgICAzXigyKT05LCAzXigzKT0yNywg
M14oNCk9ODEsIDNeKDUpPTI0Mw0KICAgIDReKDIpPTE2LCA0XigzKT02NCwgNF4o
NCk9MjU2LCA0Xig1KT0xMDI0DQogICAgNV4oMik9MjUsIDVeKDMpPTEyNSwgNV4o
NCk9NjI1LCA1Xig1KT0zMTI1DQoNCklmIHRoZXkgYXJlIHRoZW4gcGxhY2VkIGlu
IG51bWVyaWNhbCBvcmRlciwgd2l0aCBhbnkgcmVwZWF0cyByZW1vdmVkLCB3ZSBn
ZXQgdGhlIGZvbGxvd2luZyBzZXF1ZW5jZSBvZiAxNSBkaXN0aW5jdCB0ZXJtczoN
Cg0KNCwgOCwgOSwgMTYsIDI1LCAyNywgMzIsIDY0LCA4MSwgMTI1LCAyNDMsIDI1
NiwgNjI1LCAxMDI0LCAzMTI1DQoNCkhvdyBtYW55IGRpc3RpbmN0IHRlcm1zIGFy
ZSBpbiB0aGUgc2VxdWVuY2UgZ2VuZXJhdGVkIGJ5IGFeKGIpIGZvciAyIDw9IGEg
PD0gMTAwIGFuZCAyIDw9IGIgPD0gMTAwPw==
*/

#include <stdint.h>
#include <gmp.h>
#include <stdio.h>

#define NUMBERTRACKERMAX 12000                  /*!< The maximum number of numbers that can be tracked */

static mpz_t collection[NUMBERTRACKERMAX];      /*!< Array of previously encountered integers */
static uint32_t xcount = 0;                     /*!< Place to add next new element */

int32_t PreviouslyEncountered(mpz_t n)
{
  uint32_t i;

  /* Search thru all the numbers encoountered so far see if there is a match */
  for(i = 0; i < xcount; i++)
  {
    /* If value has been seen before */
    if(mpz_cmp(n, collection[i]) == 0)
    {
      return 1;
    }
  }

  /* Add the number to encountered values if there is space */
  if(xcount < NUMBERTRACKERMAX )
  {
    mpz_set(collection[xcount], n);
    xcount++;
  }
  else
  {
    return NUMBERTRACKERMAX ;
  }

  return 0;

} /* PreviouslyEncountered */

int32_t CountPreviouslyEncountered(void)
{
  return xcount;
} /* CountPreviouslyEncountered */

int main()
{
  uint32_t a,b;
  mpz_t r;

  for (a = 0; a < NUMBERTRACKERMAX; a++)
  {
    mpz_init(collection[a]);
  }
  mpz_init(r);

  for (a = 2; a <= 100; a++)
  {
    for (b = 2; b <= 100; b++)
    {
      mpz_ui_pow_ui(r, a, b);
      PreviouslyEncountered(r);
    }
  }

  printf("%u\n", CountPreviouslyEncountered());

  for (a = 0; a < NUMBERTRACKERMAX; a++)
  {
    mpz_clear(collection[a]);
  }
  mpz_clear(r);

  return 0;
}


