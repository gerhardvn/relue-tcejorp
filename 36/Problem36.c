/* 
VGhlIGRlY2ltYWwgbnVtYmVyLCA1ODUgPSAxMDAxMDAxMDAxMiAoYmluYXJ5KSwg
aXMgcGFsaW5kcm9taWMgaW4gYm90aCBiYXNlcy4NCg0KRmluZCB0aGUgc3VtIG9m
IGFsbCBudW1iZXJzLCBsZXNzIHRoYW4gb25lIG1pbGxpb24sIHdoaWNoIGFyZSBw
YWxpbmRyb21pYyBpbiBiYXNlIDEwIGFuZCBiYXNlIDIuDQoNCihQbGVhc2Ugbm90
ZSB0aGF0IHRoZSBwYWxpbmRyb21pYyBudW1iZXIsIGluIGVpdGhlciBiYXNlLCBt
YXkgbm90IGluY2x1ZGUgbGVhZGluZyB6ZXJvcy4p
*/

#include "../EulerLib/euler.h"
#include <stdio.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int HowManyBinaryDigits(unsigned long long int n)
{
  int i = 1;
  int digits = 0;

  do
  {
    i *= 2;
    digits++;
  }while ((n / i) != 0);

  return digits;
}

int IsBitSet(unsigned long long int var, unsigned int ind)
{
  return ((var & (1 << ind)) != 0);
}

int IsBinaryPalindrome(unsigned long long int n)
{
  int i = 0;
  int digits = HowManyBinaryDigits(n);

  for (i = 0; i < (digits / 2); i++)
  {
    if(IsBitSet(n,i) != IsBitSet(n,(digits - i - 1)))
    {
      return 0;
    }
  }

  return 1;
}

int main()
{
  // clock_t start = clock();
  unsigned long long int i = 1;
  unsigned int sum = 0;

  while (i < 1000000)
  {
    if((IsPalindrome(i)) && (IsBinaryPalindrome(i)))
    {
      // printf("%llu \n",i);
      sum += i;
    }
    i ++;
  }
  printf("%u \n",sum);

  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}