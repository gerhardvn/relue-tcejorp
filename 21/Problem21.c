/*
TGV0IGQobikgYmUgZGVmaW5lZCBhcyB0aGUgc3VtIG9mIHByb3BlciBkaXZpc29y
cyBvZiBuIChudW1iZXJzIGxlc3MgdGhhbiBuDQp3aGljaCBkaXZpZGUgZXZlbmx5
IGludG8gbikuDQpJZiBkKGEpID0gYiBhbmQgZChiKSA9IGEsIHdoZXJlIGEgPyBi
LCB0aGVuIGEgYW5kIGIgYXJlIGFuIGFtaWNhYmxlIHBhaXIgYW5kDQplYWNoIG9m
IGEgYW5kIGIgYXJlIGNhbGxlZCBhbWljYWJsZSBudW1iZXJzLg0KDQpGb3IgZXhh
bXBsZSwgdGhlIHByb3BlciBkaXZpc29ycyBvZiAyMjAgYXJlDQoxLCAyLCA0LCA1
LCAxMCwgMTEsIDIwLCAyMiwgNDQsIDU1IGFuZCAxMTA7IHRoZXJlZm9yZSBkKDIy
MCkgPSAyODQuDQpUaGUgcHJvcGVyIGRpdmlzb3JzIG9mIDI4NCBhcmUgMSwgMiwg
NCwgNzEgYW5kIDE0Mjsgc28gZCgyODQpID0gMjIwLg0KDQpFdmFsdWF0ZSB0aGUg
c3VtIG9mIGFsbCB0aGUgYW1pY2FibGUgbnVtYmVycyB1bmRlciAxMDAwMC4=
*/

#include <stdint.h>
#include "../EulerLib/euler.h"
#include <stdio.h>
#include <inttypes.h>

uint32_t ArraySum(uint32_t a[128], uint32_t d)
{
  unsigned int n = 0;
  unsigned int i;

  for (i = 0; i < d; i++)
  {
    n += a[i];
  }
  return n;
}

int main()
{
  uint32_t i,j;
  unsigned int answer = 0;

  uint32_t iFactors[128];
  uint32_t jFactors[128];
  uint32_t iFactorCount;
  uint32_t jFactorCount;

  for (i = 1; i < 10000; i++)
  {
    iFactorCount = Factorise(i, &iFactors[0]);
    j = ArraySum(iFactors, iFactorCount);

    if(i < j)
    {
      jFactorCount = Factorise(j, &jFactors[0]);
      if(ArraySum(jFactors, jFactorCount) == i)
      {
        answer += i + j;
        //printf("%"PRIu32" -> %"PRIu32"\n",i,j);
      }
    }
  }

  printf("%u\n",answer);

  return 0;
}
