#include "Problem55.c"

int main()
{
  mpz_t testNumber;
  mpz_init(testNumber);
  mpz_set_ui(testNumber,1234);
  mpz_t temp;
  mpz_init(temp);
  mpz_set(temp, testNumber);

  mpz_t rev;
  mpz_init(rev);
  ReverseNumber(temp, rev);
  gmp_printf("%Zd %Zd\n",testNumber, rev);

  mpz_set(temp, testNumber);
  unsigned res =  IsPalindrome(temp);
  gmp_printf("%Zd",testNumber);
 
  printf("(%d)",res);
  if (res == 0)
  {
    printf("+\n"); 
  }
  else
  {
    printf("-\n"); 
  }

  mpz_set_ui(testNumber,12321); 
  mpz_set(temp, testNumber);
  res =  IsPalindrome(temp);
  gmp_printf("%Zd",testNumber);
 
  printf("(%d)",res);
  if (res == 0)
  {
    printf("+\n"); 
  }
  else
  {
    printf("-\n"); 
  }

  
}
