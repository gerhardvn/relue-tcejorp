/*
If we take 47, reverse and add, 47 + 74 = 121, which is palindromic.

Not all numbers produce palindromes so quickly. For example,

349 + 943 = 1292,
1292 + 2921 = 4213
4213 + 3124 = 7337

That is, 349 took three iterations to arrive at a palindrome.

Although no one has proved it yet, it is thought that some numbers, like 196, never produce a palindrome. A number that never forms a palindrome through the reverse and add process is called a Lychrel number. Due to the theoretical nature of these numbers, and for the purpose of this problem, we shall assume that a number is Lychrel until proven otherwise. In addition you are given that for every number below ten-thousand, it will either (i) become a palindrome in less than fifty iterations, or, (ii) no one, with all the computing power that exists, has managed so far to map it to a palindrome. In fact, 10677 is the first number to be shown to require over fifty iterations before producing a palindrome: 4668731596684224866951378664 (53 iterations, 28-digits).

Surprisingly, there are palindromic numbers that are themselves Lychrel numbers; the first example is 4994.

How many Lychrel numbers are there below ten-thousand?

NOTE: Wording was modified slightly on 24 April 2007 to emphasise the theoretical nature of Lychrel numbers.
*/

/*
Take any positive integer of two digits or more, reverse the digits, and add to the original number. This is the operation of the reverse-then-add sequence. Now repeat the procedure with the sum so obtained until a palindromic number is obtained. This procedure quickly produces palindromic numbers for most integers. For example, starting with the number 5280 produces the sequence 5280, 6105, 11121, 23232. The end results of applying the algorithm to 1, 2, 3, ... are 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 11, 33, 44, 55, 66, 77, 88, 99, 121, ... (OEIS A033865). The value for 89 is especially large, being 8813200023188.

The first few numbers not known to produce palindromes, sometimes known as Lychrel numbers (VanLandingham), are 196, 295, 394, 493, 592, 689, 691, 788, 790, 879, 887, ... (OEIS A023108).
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <gmp.h>

void ReverseNumber(mpz_t number, mpz_t reverse)
{
  mpz_t temp;
  mpz_init(temp);
  
  while (mpz_cmp_ui(number, 0) == 1)
  {
    unsigned remainder = mpz_mod_ui(temp,number,10);
    mpz_mul_ui (reverse, reverse, 10);
    mpz_add_ui(reverse, reverse, remainder);
    mpz_fdiv_q_ui(number, number, 10);
  }
}
 
int IsPalindrome(mpz_t number)
{
  mpz_t rumber;
  mpz_init(rumber);
  mpz_t temp;
  mpz_init(temp);
  mpz_set(temp, number);
 
  ReverseNumber(temp, rumber);
  
  return mpz_cmp(number, rumber);
}

int IsLychrel(int number)
{
  mpz_t testNumber;
  mpz_init(testNumber);
  mpz_set_ui(testNumber,number);
  

  for (int i = 0; i < 50; i++)
  {
    mpz_t temp;
    mpz_init(temp);
    mpz_set(temp, testNumber);
    
    int res = IsPalindrome(temp);
    
    gmp_printf("(%d)%Zd",i,testNumber);
    
    if (res == 0)
    {
      printf("+\n");
      return 0;
    }

    mpz_set(temp, testNumber);
    mpz_t rev;
    mpz_init(rev);
    
    ReverseNumber(temp, rev);
    
    mpz_add(testNumber, rev, testNumber);
  }
  puts("");
  return 1;
}

int main()
{
  
  const int start = 10;
  const int limit = 10000;
  int result = 0;
 
  for (int i = start; i < limit; i++)
  {
    printf ("%d: ",i);
    if (IsLychrel(i))
    {
      result++;
    }
  }
  printf("Result: %u\n",result);
}

