/*
U3VycHJpc2luZ2x5IHRoZXJlIGFyZSBvbmx5IHRocmVlIG51bWJlcnMgdGhhdCBj
YW4gYmUgd3JpdHRlbiBhcyB0aGUgc3VtIG9mIGZvdXJ0aCBwb3dlcnMgb2YgdGhl
aXIgZGlnaXRzOg0KDQoxNjM0ID0gMTQgKyA2NCArIDM0ICsgNDQNCjgyMDggPSA4
NCArIDI0ICsgMDQgKyA4NA0KOTQ3NCA9IDk0ICsgNDQgKyA3NCArIDQ0DQpBcyAx
ID0gMTQgaXMgbm90IGEgc3VtIGl0IGlzIG5vdCBpbmNsdWRlZC4NCg0KVGhlIHN1
bSBvZiB0aGVzZSBudW1iZXJzIGlzIDE2MzQgKyA4MjA4ICsgOTQ3NCA9IDE5MzE2
Lg0KDQpGaW5kIHRoZSBzdW0gb2YgYWxsIHRoZSBudW1iZXJzIHRoYXQgY2FuIGJl
IHdyaXR0ZW4gYXMgdGhlIHN1bSBvZiBmaWZ0aCBwb3dlcnMgb2YgdGhlaXIgZGln
aXRzLg==
*/

#include "../EulerLib/euler.h"
#include <stdio.h>
#include <math.h>

int main()
{
  unsigned int i,j;
  unsigned int sum;
  unsigned int answer = 0;

  for (i = 10; i < 1000000; i++)
  {
    sum = 0;
    for (j = 1; j <= HowManyDigits(i); j++)
    {
      sum += pow(GetDigit(i,j),5);
    }
    if(sum == i)
    {
      // printf("%u\n",i);
      answer += i;
    }
  }

  printf("%u\n",answer);

  return 0;
}

