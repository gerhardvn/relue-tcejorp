/*
UGVybXV0ZWQgbXVsdGlwbGVzDQpJdCBjYW4gYmUgc2VlbiB0aGF0IHRoZSBudW1i
ZXIsIDEyNTg3NCwgYW5kIGl0cyBkb3VibGUsIDI1MTc0OCwgY29udGFpbiBleGFj
dGx5IHRoZSBzYW1lIGRpZ2l0cywgYnV0IGluIGEgZGlmZmVyZW50IG9yZGVyLg0K
RmluZCB0aGUgc21hbGxlc3QgcG9zaXRpdmUgaW50ZWdlciwgeCwgc3VjaCB0aGF0
IDJ4LCAzeCwgNHgsIDV4LCBhbmQgNngsIGNvbnRhaW4gdGhlIHNhbWUgZGlnaXRz
Lg0K
*/


#include "../EulerLib/euler.h"
#include <stdio.h>
#include <stdlib.h>

uint32_t IsPermutation(uint32_t a, uint32_t b)
{
	/* count the occurrence of each digit of a and b. They are permutation
	   if they have the same number of occurrence of each digit */
	unsigned digitsA[10] = {0};
  unsigned digitsB[10] = {0};

	while (a > 0)
	{
		digitsA[a%10]++;
		a /= 10;
	}

	while (b > 0)
	{
		digitsB[b%10]++;
		b /= 10;
	}

	for (unsigned i = 0; i < 10; i++)
  {
		if (digitsA[i] != digitsB[i])
    {
			return 0;
    }
  }

	return 1;
}

int main()
{
  unsigned x = 0;
  unsigned digits;
 
  do
  {
    x++;
    digits = HowManyDigits(x);
    
    unsigned x2 = x*2;
    unsigned x3 = x*3;
    unsigned x4 = x*4;
    unsigned x5 = x*5;
    unsigned x6 = x*6;
    
    if((digits == HowManyDigits(x2)) &&
       (digits == HowManyDigits(x3)) &&
       (digits == HowManyDigits(x4)) &&
       (digits == HowManyDigits(x5)) &&
       (digits == HowManyDigits(x6)))
    {
      if((IsPermutation(x,x2)) &&
         (IsPermutation(x,x3)) &&
         (IsPermutation(x,x4)) &&
         (IsPermutation(x,x5)) &&
         (IsPermutation(x,x6)))
      {
        printf("%u\n", x);
      }
    }
  }while (digits < 7);
}
