/* 
MTQ1IGlzIGEgY3VyaW91cyBudW1iZXIsIGFzIDEhICsgNCEgKyA1ISA9IDEgKyAy
NCArIDEyMCA9IDE0NS4NCg0KRmluZCB0aGUgc3VtIG9mIGFsbCBudW1iZXJzIHdo
aWNoIGFyZSBlcXVhbCB0byB0aGUgc3VtIG9mIHRoZSBmYWN0b3JpYWwgb2YgdGhl
aXIgZGlnaXRzLg0KDQpOb3RlOiBhcyAxISA9IDEgYW5kIDIhID0gMiBhcmUgbm90
IHN1bXMgdGhleSBhcmUgbm90IGluY2x1ZGVkLg==
*/

#include "../EulerLib/euler.h"
#include <stdio.h>
#include <math.h>

int main()
{
  unsigned int i,j;
  unsigned int sum;
  unsigned int answer = 0;

  for (i = 10; i < 2000000; i++)
  {
    sum = 0;
    for (j = 1; j <= HowManyDigits(i); j++)
    {
      sum += factorial(GetDigit(i,j));
    }
    if(sum == i)
    {
      //printf("%u\n",i);
      answer += i;
    }
  }

  printf("%u\n",answer);

  return 0;
}

