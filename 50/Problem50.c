/*
VGhlIHByaW1lIDQxLCBjYW4gYmUgd3JpdHRlbiBhcyB0aGUgc3VtIG9mIHNpeCBj
b25zZWN1dGl2ZSBwcmltZXM6DQo0MSA9IDIgKyAzICsgNSArIDcgKyAxMSArIDEz
DQoNClRoaXMgaXMgdGhlIGxvbmdlc3Qgc3VtIG9mIGNvbnNlY3V0aXZlIHByaW1l
cyB0aGF0IGFkZHMgdG8gYSBwcmltZSBiZWxvdyBvbmUtaHVuZHJlZC4NCg0KVGhl
IGxvbmdlc3Qgc3VtIG9mIGNvbnNlY3V0aXZlIHByaW1lcyBiZWxvdyBvbmUtdGhv
dXNhbmQgdGhhdCBhZGRzIHRvIGEgcHJpbWUsIGNvbnRhaW5zIDIxIHRlcm1zLCBh
bmQgaXMgZXF1YWwgdG8gOTUzLg0KDQpXaGljaCBwcmltZSwgYmVsb3cgb25lLW1p
bGxpb24sIGNhbiBiZSB3cml0dGVuIGFzIHRoZSBzdW0gb2YgdGhlIG1vc3QgY29u
c2VjdXRpdmUgcHJpbWVzPw==
*/

#include "../EulerLib/euler.h"
#include <stdio.h>


int main()
{
  unsigned long long sum;
  unsigned i;
  unsigned long long nextPrime;
  unsigned max_i = 0;
  unsigned long long answer;

  do
  {
    nextPrime = GetNextPrime();
    sum = nextPrime;
    i = 1;

    do
    {
      nextPrime = ToNextPrime(nextPrime);
      sum += nextPrime;
      i++;
      if(isPrime(sum))
      {
        if (i > max_i)
        {
          answer = sum;
          max_i = i;
          //printf("(%u), %llu\n", i, sum);
        }
      }
    }while(sum < 1000000);

  }while(nextPrime < 1000000);

  printf("%llu\n", answer);
  
  return 0;
}