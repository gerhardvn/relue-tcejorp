/*
VGhlIG50aCB0ZXJtIG9mIHRoZSBzZXF1ZW5jZSBvZiB0cmlhbmdsZSBudW1iZXJz
IGlzIGdpdmVuIGJ5LCB0biA9IMK9bihuKzEpOyBzbyB0aGUgZmlyc3QgdGVuIHRy
aWFuZ2xlIG51bWJlcnMgYXJlOg0KDQoxLCAzLCA2LCAxMCwgMTUsIDIxLCAyOCwg
MzYsIDQ1LCA1NSwgLi4uDQoNCkJ5IGNvbnZlcnRpbmcgZWFjaCBsZXR0ZXIgaW4g
YSB3b3JkIHRvIGEgbnVtYmVyIGNvcnJlc3BvbmRpbmcgdG8gaXRzIGFscGhhYmV0
aWNhbCBwb3NpdGlvbiBhbmQgYWRkaW5nIHRoZXNlIHZhbHVlcyB3ZSBmb3JtIGEg
d29yZCB2YWx1ZS4gRm9yIGV4YW1wbGUsIHRoZSB3b3JkIHZhbHVlIGZvciBTS1kg
aXMgMTkgKyAxMSArIDI1ID0gNTUgPSB0MTAuIElmIHRoZSB3b3JkIHZhbHVlIGlz
IGEgdHJpYW5nbGUgbnVtYmVyIHRoZW4gd2Ugc2hhbGwgY2FsbCB0aGUgd29yZCBh
IHRyaWFuZ2xlIHdvcmQuDQoNClVzaW5nIHdvcmRzLnR4dCAocmlnaHQgY2xpY2sg
YW5kICdTYXZlIExpbmsvVGFyZ2V0IEFzLi4uJyksIGEgMTZLIHRleHQgZmlsZSBj
b250YWluaW5nIG5lYXJseSB0d28tdGhvdXNhbmQgY29tbW9uIEVuZ2xpc2ggd29y
ZHMsIGhvdyBtYW55IGFyZSB0cmlhbmdsZSB3b3Jkcz8=
*/

#include "../EulerLib/euler.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

char * LoadWord(FILE *namefile)
{
  char buf[256];
  char ch;
  unsigned int i = 0;
  int ReadName = 0;
  char * s = NULL;

  do
  {

    ch = fgetc(namefile);
    buf[i] = ch;

    if((ch >= 'A') && (ch <= 'Z'))
    {
      i++;
      ReadName = 1;
    }
    else if(ReadName)
    {
      ch = EOF;
    }

  } while (ch != EOF);

  if(i)
  {
    buf[i] = 0;
    s = malloc(i + 1);
    strcpy(s,buf);
  }

  return s;

}

int main()
{
  FILE *numfile;
  char * names[10000];

  unsigned i = 0;

  unsigned int answer = 0;
  unsigned int score = 0;
  unsigned int count = 0;
  unsigned int length = 0;

  numfile = fopen("words.txt", "r");

  if (numfile != NULL)
  {

    do
    {
      names[i] = LoadWord(numfile);
      i++;
    }while (names[i - 1] != NULL);

    fclose(numfile);

    while(count < (i-1))
    {
      length = strlen(names[count]);

      score = 0;
      for (unsigned j = 0; j < length; j++)
      {
        score += (((names[count])[j]) - ('A' - 1));
      }

      count++;

      if(isTriangleNumber(score) == 1)
      {
        answer ++;
        //printf("%4u: %20s : %8u\n", answer, names[count-1], score);
      }

      free(names[count-1]);

    }

    //fflush(stdout);

    printf("%u\n",answer);

  }

  return 0;
}
