/*
QSBwZXJtdXRhdGlvbiBpcyBhbiBvcmRlcmVkIGFycmFuZ2VtZW50IG9mIG9iamVj
dHMuIEZvciBleGFtcGxlLCAzMTI0IGlzIG9uZQ0KcG9zc2libGUgcGVybXV0YXRp
b24gb2YgdGhlIGRpZ2l0cyAxLCAyLCAzIGFuZCA0LiBJZiBhbGwgb2YgdGhlIHBl
cm11dGF0aW9ucw0KYXJlIGxpc3RlZCBudW1lcmljYWxseSBvciBhbHBoYWJldGlj
YWxseSwgd2UgY2FsbCBpdCBsZXhpY29ncmFwaGljIG9yZGVyLiBUaGUNCmxleGlj
b2dyYXBoaWMgcGVybXV0YXRpb25zIG9mIDAsIDEgYW5kIDIgYXJlOg0KDQowMTIg
ICAwMjEgICAxMDIgICAxMjAgICAyMDEgICAyMTANCg0KV2hhdCBpcyB0aGUgbWls
bGlvbnRoIGxleGljb2dyYXBoaWMgcGVybXV0YXRpb24gb2YgdGhlIGRpZ2l0cyAw
LCAxLCAyLCAzLCA0LA0KNSwgNiwgNywgOCBhbmQgOT8=
*/

#include <stdint.h>
#include "../EulerLib/euler.h"
#include <stdio.h>

void print(const unsigned int *v, const int size)
{
  int i;
  if (v != 0)
  {
    for (i = 0; i < size; i++)
    {
      printf("%d", v[i] );
    }
    printf("\n");
  }
}


void swap(unsigned int *v, const int i, const int j)
{
  int t;
  t = v[i];
  v[i] = v[j];
  v[j] = t;
}

void rotateLeft(unsigned int *v, const int start, const int n)
{
  int i;
  int tmp = v[start];
  for (i = start; i < n-1; i++) {
    v[i] = v[i+1];
  }
  v[n-1] = tmp;
} // rotateLeft


void permute(unsigned int *v, const int start, const int n, unsigned int *c)
{
  int i, j;

  (*c)--;

  if (*c == 0)
  {
    print(v, n);
    return;
  }

  if (start < n)
  {
    for (i = n-2; i >= start; i--)
    {
      for (j = i + 1; j < n; j++)
      {
        swap(v, i, j);
        permute(v, i+1, n, c);
      }
      rotateLeft(v, i, n);
    }
  }
} // permute

int main()
{
  unsigned int a[10] = {0,1,2,3,4,5,6,7,8,9};
  unsigned int c = 1000000;

  permute(&a[0],0,10, &c);
  return 0;
}
