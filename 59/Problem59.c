/*
Each character on a computer is assigned a unique code and the preferred standard is ASCII (American Standard Code for Information Interchange). For example, uppercase A = 65, asterisk (*) = 42, and lowercase k = 107.

A modern encryption method is to take a text file, convert the bytes to ASCII, then XOR each byte with a given value, taken from a secret key. The advantage with the XOR function is that using the same encryption key on the cipher text, restores the plain text; for example, 65 XOR 42 = 107, then 107 XOR 42 = 65.

For unbreakable encryption, the key is the same length as the plain text message, and the key is made up of random bytes. The user would keep the encrypted message and the encryption key in different locations, and without both "halves", it is impossible to decrypt the message.

Unfortunately, this method is impractical for most users, so the modified method is to use a password as a key. If the password is shorter than the message, which is likely, the key is repeated cyclically throughout the message. The balance for this method is using a sufficiently long password key for security, but short enough to be memorable.

Your task has been made easy, as the encryption key consists of three lower case characters. Using p059_cipher.txt (right click and 'Save Link/Target As...'), a file containing the encrypted ASCII codes, and the knowledge that the plain text must contain common English words, decrypt the message and find the sum of the ASCII values in the original text.
*/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>

#define FILE_WITH_CRYPT "./p059_cipher.txt"

// Return the size of a file
off_t fsize(const char *filename)
{
  struct stat st;

  // Get File Size
  if (stat(filename, &st) == 0)
  {
    return st.st_size;
  }

  fprintf(stderr, "Cannot determine size of %s: %s\n", filename, strerror(errno));

  return -1;
}

// Convert integer array to character array and print.
void print_array_as_string(int *array, int size)
{
  int string_len = 0;

  char *string = malloc((size+1) * sizeof(*string));
  
  if (!string)
  {
    printf("Cannot allocate memory\n");
    exit(EXIT_FAILURE);
  }

  for (int i = 0; i < size; i++)
  {
    string[string_len++] = array[i];
  }
  string[string_len] = '\0';

  printf("%s\n",string);
  free(string);
  return;
}

int ascii_sum(int *array, int size)
{
  int sum = 0;

  for (int i = 0; i < size; i++)
  {
    sum += array[i];
  }
  return sum;
}

// Lacks proper frequency analysis but it is good enough for now.
int decrypt_3(int *crypt_array, int size, int *decrypt_array, int key[3])
{
  int white_space = 0;
  int special = 0;
  
  for (int i = 0; i < size; i++)
  {
    int x = (i % 3);
    decrypt_array[i] = (key[x] ^ crypt_array[i]) & 0xff;
    if ((decrypt_array[i] >= 0x80) || (decrypt_array[i] == 0))
    {
      return 0;
    }
    else if ((decrypt_array[i] <= 0x09))
    {
      return 0;
    }
    else if ((decrypt_array[i] <= 0x20))
    {
      white_space++;
    }
    else if ((decrypt_array[i] <= 0x40))
    {
      special++;
    }
  }
  
  if((white_space) > (size / 4))
  {
    return 0;
  }
  
  if((special) > (size / 8))
  {
    return 0;
  }

  return 1;
}

int decrypt(int *crypt_array, int size, int *decrypt_array, int key)
{
  for (int i = 0; i < size; i++)
  {
    decrypt_array[i] = (key ^ crypt_array[i]) & 0xff;
    if ((decrypt_array[i] >= 128) || (decrypt_array[i] == 0))
    {
      return 0;
    }
  }
  return 1;
}

int main()
{
  FILE *crypt;
  crypt = fopen(FILE_WITH_CRYPT, "r");
  
  if (!crypt)
  {  
    fprintf(stderr, "File open failed %s: %s\n", FILE_WITH_CRYPT, strerror(errno));
    return 1;
  }
  
  // size with spare
  off_t crypt_length = fsize(FILE_WITH_CRYPT) / 2; 
  
  //read file into array
  int *crypt_array = (int *) malloc(sizeof(int) * crypt_length);

  // Read file into array
  int i = 0;
  while (fscanf(crypt, "%2d,", &crypt_array[i]) == 1)
  {
    i++;
  }

  crypt_length = i;
  int * decrypt_array =  (int *) malloc(sizeof(int) * crypt_length);

  int key[3];
  
  // Loop through all possible keys
  for (int a = 0x61; a < 0x7b; a++)
  {
    key[2] = a;
    for (int b = 0x61; b < 0x7b; b++)
    {
      key[1] = b;
      for (int c = 0x61; c < 0x7b; c++)
      {
        key[0] = c;
        if(decrypt_3(crypt_array,crypt_length,decrypt_array, key) == 1)
        {
          print_array_as_string(decrypt_array, crypt_length);
          printf("sum = %d\n",ascii_sum(decrypt_array, crypt_length));
        }
      }
    }
  }
  
  free(crypt_array);
  free(decrypt_array);
}    