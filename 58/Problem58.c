/*
U3RhcnRpbmcgd2l0aCAxIGFuZCBzcGlyYWxsaW5nIGFudGljbG9ja3dpc2UgaW4g
dGhlIGZvbGxvd2luZyB3YXksIGEgc3F1YXJlIHNwaXJhbCB3aXRoIHNpZGUgbGVu
Z3RoIDcgaXMgZm9ybWVkLg0KDQozNyAzNiAzNSAzNCAzMyAzMiAzMQ0KMzggMTcg
MTYgMTUgMTQgMTMgMzANCjM5IDE4ICA1ICA0ICAzIDEyIDI5DQo0MCAxOSAgNiAg
MSAgMiAxMSAyOA0KNDEgMjAgIDcgIDggIDkgMTAgMjcNCjQyIDIxIDIyIDIzIDI0
IDI1IDI2DQo0MyA0NCA0NSA0NiA0NyA0OCA0OQ0KDQpJdCBpcyBpbnRlcmVzdGlu
ZyB0byBub3RlIHRoYXQgdGhlIG9kZCBzcXVhcmVzIGxpZSBhbG9uZyB0aGUgYm90
dG9tIHJpZ2h0IGRpYWdvbmFsLCBidXQgd2hhdCBpcyBtb3JlIGludGVyZXN0aW5n
IGlzIHRoYXQgOCBvdXQgb2YgdGhlIDEzIG51bWJlcnMgbHlpbmcgYWxvbmcgYm90
aCBkaWFnb25hbHMgYXJlIHByaW1lOyB0aGF0IGlzLCBhIHJhdGlvIG9mIDgvMTMg
4omIIDYyJS4NCg0KSWYgb25lIGNvbXBsZXRlIG5ldyBsYXllciBpcyB3cmFwcGVk
IGFyb3VuZCB0aGUgc3BpcmFsIGFib3ZlLCBhIHNxdWFyZSBzcGlyYWwgd2l0aCBz
aWRlIGxlbmd0aCA5IHdpbGwgYmUgZm9ybWVkLiBJZiB0aGlzIHByb2Nlc3MgaXMg
Y29udGludWVkLCB3aGF0IGlzIHRoZSBzaWRlIGxlbmd0aCBvZiB0aGUgc3F1YXJl
IHNwaXJhbCBmb3Igd2hpY2ggdGhlIHJhdGlvIG9mIHByaW1lcyBhbG9uZyBib3Ro
IGRpYWdvbmFscyBmaXJzdCBmYWxscyBiZWxvdyAxMCU/DQo=
*/

#include <stdint.h>
#include "../EulerLib/euler.h"
#include <stdio.h>
#include <string.h>
#include <stddef.h>

int main()
{
  unsigned diag = 1;
  unsigned step = 2;
  unsigned ndiag = 0;
  unsigned prime = 0;
 
  do
  {
    for (int x = 0; x < 4; x++)
    {
      if(isPrime(diag))
      {
        prime ++;
      }
      ndiag++;
      diag += step;
    }
    step += 2;
    
  }while (((float)prime/(float)ndiag) > 0.1);
    
  printf("%u\n",(step - 3));  
}    