/*
RWFjaCBuZXcgdGVybSBpbiB0aGUgRmlib25hY2NpIHNlcXVlbmNlIGlzIGdlbmVy
YXRlZCBieSBhZGRpbmcgdGhlIHByZXZpb3VzDQp0d28gdGVybXMuIEJ5IHN0YXJ0
aW5nIHdpdGggMSBhbmQgMiwgdGhlIGZpcnN0IDEwIHRlcm1zIHdpbGwgYmU6DQoN
CjEsIDIsIDMsIDUsIDgsIDEzLCAyMSwgMzQsIDU1LCA4OSwgLi4uDQoNCkZpbmQg
dGhlIHN1bSBvZiBhbGwgdGhlIGV2ZW4tdmFsdWVkIHRlcm1zIGluIHRoZSBzZXF1
ZW5jZSB3aGljaCBkbyBub3QgZXhjZWVkIGZvdXIgbWlsbGlvbi4=
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
  unsigned int sum = 0;
  unsigned int fbt1 = 1;
  unsigned int fbt2 = 1;
  unsigned int temp;

  while (fbt2 < 4000000)
  {
    temp = fbt2;
    fbt2 = fbt1 + fbt2;
    fbt1 = temp;

    if(((fbt2 % 2) == 0)&&(fbt2 < 4000000))
    {
      sum += fbt2;
    }

  }

  printf("%u\n",sum);
  return 0;
}
