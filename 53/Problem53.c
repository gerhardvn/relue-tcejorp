/*
VGhlcmUgYXJlIGV4YWN0bHkgdGVuIHdheXMgb2Ygc2VsZWN0aW5nIHRocmVlIGZy
b20gZml2ZSwgMTIzNDU6DQoNCjEyMywgMTI0LCAxMjUsIDEzNCwgMTM1LCAxNDUs
IDIzNCwgMjM1LCAyNDUsIGFuZCAzNDUNCg0KSW4gY29tYmluYXRvcmljcywgd2Ug
dXNlIHRoZSBub3RhdGlvbiwgKDUvMyk9MTAuDQoNCkluIGdlbmVyYWwsIChuL3Ip
PW4hL3IhKG7iiJJyKSEsIHdoZXJlIHLiiaRuLCBuIT1uw5cobuKIkjEpw5cuLi7D
lzPDlzLDlzEsIGFuZCAwIT0xLg0KDQpJdCBpcyBub3QgdW50aWwgbj0yMywgdGhh
dCBhIHZhbHVlIGV4Y2VlZHMgb25lLW1pbGxpb246ICgyMy8xMCk9MTE0NDA2Ni4N
Cg0KSG93IG1hbnksIG5vdCBuZWNlc3NhcmlseSBkaXN0aW5jdCwgdmFsdWVzIG9m
IChuL3IpIGZvciAx4omkbuKJpDEwMCwgYXJlIGdyZWF0ZXIgdGhhbiBvbmUtbWls
bGlvbj8=
*/

#include <stdio.h>

int test(unsigned n, unsigned r)
{
  const unsigned limit = 1000000;
  unsigned nr = n - r;
  unsigned long long answer = 1;

  while (n > r)
  {
    answer *= n;
    
    while ((nr > 1) && ((answer % nr == 0) || (answer > limit))) 
    {
      answer /= nr;
      nr--;
    }

    if (nr <= 1 && answer > limit)
    {
      return 1;
    }

    n--;
  }

  return 0;
}

int main()
{
  unsigned total=0;
  unsigned r, n;

  for (n = 23 ; n <= 100; n++) 
  {
    for (r = 1; r < n; r++)
    {
      if (test(n, r) == 1)
      {
        total++;
      }
    }
  }
  
  printf("%u\n", total);
  return 0;
}