#
# A palindromic number reads the same both ways. The largest palindrome made
# from the product of two 2-digit numbers is 9009 = 91 � 99.
#
# Find the largest palindrome made from the product of two 3-digit numbers.
#
def isPalindrome(n)
  digit1 = (n % 10);
  digit6 = ((n - (n % 100000))/100000);
  digit2 = ((n % 100)/10);
  digit5 = (((n - (n % 10000)) / 10000) %10);
  digit3 = ((n % 1000)/100);
  digit4 = (((n - (n % 1000)) / 1000)%10);

  if (digit1 != digit6) or (digit2 != digit5) or (digit3 != digit4)
    return false
  end

  true
end

max = 0

x = 999
while x > 100 do

  y = 999
  while y > 100 do
    if isPalindrome(x * y)
      if max < (x*y)
        max = x*y
      end
    end
    y = y - 1
  end
  x = x - 1
end

puts max