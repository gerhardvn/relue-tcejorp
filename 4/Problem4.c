/*
QSBwYWxpbmRyb21pYyBudW1iZXIgcmVhZHMgdGhlIHNhbWUgYm90aCB3YXlzLiBU
aGUgbGFyZ2VzdCBwYWxpbmRyb21lIG1hZGUgZnJvbSB0aGUgcHJvZHVjdCBvZiB0
d28gMi1kaWdpdCBudW1iZXJzIGlzIDkwMDkgPSA5MSDDlyA5OS4NCkZpbmQgdGhl
IGxhcmdlc3QgcGFsaW5kcm9tZSBtYWRlIGZyb20gdGhlIHByb2R1Y3Qgb2YgdHdv
IDMtZGlnaXQgbnVtYmVycy4NCg==
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdint.h>
#include "../EulerLib/euler.h"

int main()
{
  // clock_t start = clock();

  unsigned palindrome = 0;
  unsigned int i,j,n;

  for(i = 999; i > 100; i--)
  {
    for(j = 999; j > 100; j--)
    {
      n = i * j;
      if(IsPalindrome(n))
      {
        // printf("%06u %03u %03u\n",n, i, j);
        if(palindrome < n)
        {
          palindrome = n;
        }
      }
    }
  }

  printf("%06u\n",palindrome);

  // printf("Time elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}