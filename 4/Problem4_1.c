/*
QSBwYWxpbmRyb21pYyBudW1iZXIgcmVhZHMgdGhlIHNhbWUgYm90aCB3YXlzLiBU
aGUgbGFyZ2VzdCBwYWxpbmRyb21lIG1hZGUgZnJvbSB0aGUgcHJvZHVjdCBvZiB0
d28gMi1kaWdpdCBudW1iZXJzIGlzIDkwMDkgPSA5MSDDlyA5OS4NCkZpbmQgdGhl
IGxhcmdlc3QgcGFsaW5kcm9tZSBtYWRlIGZyb20gdGhlIHByb2R1Y3Qgb2YgdHdv
IDMtZGlnaXQgbnVtYmVycy4NCg==
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int SixDigitPalindromeCheck(unsigned int n)
{
  int digit1 = (n % 10);
  int digit6 = ((n - (n % 100000))/100000);
  int digit2 = ((n % 100)/10);
  int digit5 = (((n - (n % 10000)) / 10000) %10);
  int digit3 = ((n % 1000)/100);
  int digit4 = (((n - (n % 1000)) / 1000)%10);

  if(digit1 != digit6)
  {
    return 0;
  }

  if(digit2 != digit5)
  {
    return 0;
  }

  if(digit3 != digit4)
  {
    return 0;
  }


  return 1;
}

int main()
{
  // clock_t start = clock();

  unsigned palindrome = 0;
  unsigned int i,j,n;

  for(i = 999; i > 100; i--)
  {
    for(j = 999; j > 100; j--)
    {
      n = i * j;
      if(SixDigitPalindromeCheck(n))
      {
        // printf("%06u %03u %03u\n",n, i, j);
        if(palindrome < n)
        {
          palindrome = n;
        }
      }
    }
  }

  printf("%06u\n",palindrome);

  // printf("Time elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}