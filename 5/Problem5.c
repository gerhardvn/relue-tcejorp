/*
MjUyMCBpcyB0aGUgc21hbGxlc3QgbnVtYmVyIHRoYXQgY2FuIGJlIGRpdmlkZWQg
YnkgZWFjaCBvZiB0aGUgbnVtYmVycyBmcm9tIDEgdG8gMTAgd2l0aG91dCBhbnkg
cmVtYWluZGVyLg0KV2hhdCBpcyB0aGUgc21hbGxlc3QgbnVtYmVyIHRoYXQgaXMg
ZXZlbmx5IGRpdmlzaWJsZSBieSBhbGwgb2YgdGhlIG51bWJlcnMgZnJvbSAxIHRv
IDIwPw0K
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <inttypes.h>

unsigned int SequenceDivision(uint64_t n)
{
  unsigned int i =3;

  while ((n % i) == 0)
  {
    i++;
  }

  return (i-1);
}

int main()
{
  // clock_t start = clock();

  uint64_t i = 2;

  while(SequenceDivision(i) < 20)
  {
    i += 2;
  }

  printf("%" PRIu64 "\n",i);

  // printf("Time elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}