/*
VGhlIDUtZGlnaXQgbnVtYmVyLCAxNjgwNz03XjUsIGlzIGFsc28gYSBmaWZ0aCBwb3dlci4gU2ltaWxhcmx5LCB0aGUgOS1kaWdpdCBudW1iZXIsIDEzNDIxNzcyOD04XjksIGlzIGEgbmludGggcG93ZXIuDQoNCkhvdyBtYW55IG4tZGlnaXQgcG9zaXRpdmUgaW50ZWdlcnMgZXhpc3Qgd2hpY2ggYXJlIGFsc28gYW4gbnRoIHBvd2VyPw
*/
#include "../EulerLib/euler.h"
#include <stdio.h>
#include <math.h>

int main()
{
  int count = 0;
  for(uint32_t i = 0; i < 10; i++)
  {
    for(uint32_t j = 0; j < 50; j++)
    {
      uint64_t result = (uint64_t)pow((double)i, j);
      
      if (HowManyDigits(result) == j)
      {
        //printf("%d^%d=%llu\n",i,j,result); 
        count++;
      }
    }
  }
  printf("%d",count);
  return 0;
}
