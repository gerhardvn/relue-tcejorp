/*
Prime digit replacements
By replacing the 1st digit of the 2-digit number *3, it turns out that six of the nine possible values:
 13, 23, 43, 53, 73, and 83, are all prime.
By replacing the 3rd and 4th digits of 56**3 with the same digit,
 this 5-digit number is the first example having seven primes among the ten generated numbers,
 yielding the family: 56003, 56113, 56333, 56443, 56663, 56773, and 56993. Consequently 56003, being the first member of this family, is the smallest prime with this property.
Find the smallest prime which, by replacing part of the number (not necessarily adjacent digits) with the same digit, is part of an eight prime value family.
*/

#include "../EulerLib/euler.h"
#include <stdio.h>
#include <stdlib.h>

unsigned * replacedigits(unsigned num, unsigned start, unsigned n)
{
  unsigned * nruter = (unsigned *) malloc (sizeof(unsigned) * 10);

  for(unsigned j = 0; j <=9; j++)
  {
    nruter[j] =  ReplaceDigits(num, start, j, n);  
  }    
  return nruter;
}


int main()
{
  unsigned prime;
  do
  {
    prime = GetNextPrime();
  }while (prime < 10);
  
  unsigned max_p = 0;
  unsigned max_a = 0;
  unsigned digits;
  
  do
  {
    prime = GetNextPrime();
    digits = HowManyDigits(prime);

    //printf("\nPrime: %u\n", prime);
    
    for (unsigned i = 1; i <= digits; i++)
    {
      //printf("Digits - %u\nMasks:\n",i);
     
      for (unsigned j = 1; j <= digits; j++)
      {
        if ((j < digits)&&(((i + j) <= (digits))||((i + j) <= (digits+1))))
        {
          //printf("start %u, replace %u\n",i,j);
          
          unsigned answer = 0;          
          unsigned * res = replacedigits(prime,i,j);
          unsigned x = 0;
          
          if (((i) == digits) || ((i+j) == (digits+1)))
          {
            x++;
          }
          
          for(; x <= 9; x++)
          {
            if(isPrime(res[x]))
            {
              answer ++;
            }
          }
          if (answer > max_a)
          {
            max_p = prime;
            max_a = answer;
            printf("%u = %u  %u %u\n", max_p, max_a, i,j);
          }
          free((void *)res);
        }
      }
    }      
  }while (digits < 7);

  printf("%u:%u\n",max_p,max_a);
}
