#
#The prime factors of 13195 are 5, 7, 13 and 29.
#
#What is the largest prime factor of the number 600851475143 ?
#

def isPrime(n)
  limit = Math.sqrt(n)
  
  case n
  when 0,1,4,6,8 
    return false
  when 2,3,5,7
    return true
  end
    
  for x in 9..limit
    if n % x == 0
      return false
    end
  end
  true
end

ulimit = Math.sqrt(600851475143).floor
for x in 1..ulimit
  if 600851475143 % x == 0
    if isPrime(x)
      print x.to_s + "\n"
    end
  end
end
