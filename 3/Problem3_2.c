/*
VGhlIHByaW1lIGZhY3RvcnMgb2YgMTMxOTUgYXJlIDUsIDcsIDEzIGFuZCAyOS4N
CldoYXQgaXMgdGhlIGxhcmdlc3QgcHJpbWUgZmFjdG9yIG9mIHRoZSBudW1iZXIg
NjAwODUxNDc1MTQzID8NCg==
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <inttypes.h>

int isprime(unsigned long long int n)
{
  uint64_t i;
  uint64_t limit = sqrt(n);

  for(i = 2; i < limit; i++ )
  {
    if((n % i) == 0)
    {
      return 0;
    }
  }

  return 1;
}

int main()
{
  // clock_t start = clock();
  uint64_t number = 600851475143ll;//13195;
  uint64_t  i = 1;
  
  uint64_t limit = sqrt(number);

  while (i < limit)
  {
    if(((number % i) == 0) && (isprime(i)))
    {
      printf("%" PRIu64 "\n",i);
    }
    i++;
  }

  // printf("Time elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}