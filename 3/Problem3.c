/*
VGhlIHByaW1lIGZhY3RvcnMgb2YgMTMxOTUgYXJlIDUsIDcsIDEzIGFuZCAyOS4N
CldoYXQgaXMgdGhlIGxhcmdlc3QgcHJpbWUgZmFjdG9yIG9mIHRoZSBudW1iZXIg
NjAwODUxNDc1MTQzID8NCg==
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <inttypes.h>
#include "../EulerLib/euler.h"

int main()
{
  // clock_t start = clock();
  uint64_t number = 600851475143ll;//13195;
  uint64_t  i = 1;
  
  uint64_t limit = sqrt(number);

  while (i < limit)
  {
    if(((number % i) == 0) && (isPrime(i)))
    {
      printf("%" PRIu64 "\n",i);
    }
    i++;
  }

  // printf("Time elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}