#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int isprime(unsigned long long int n)
{
  unsigned long long int i;
  unsigned long long int limit = sqrt(n);

  for(i = 2; i < limit; i++ )
  {
    if((n % i) == 0)
    {
      return 0;
    }
  }

  return 1;
}

int GetNextPrime(void)
{
  static int prime = 1;

  prime++;

  while(isprime(prime) == 0)
  {
    prime++;
  }

  return prime;
}

int main()
{
  // clock_t start = clock();
  unsigned long long int number = 600851475143ll; //13195;
  unsigned long long int i = 1;
  unsigned long long int limit = sqrt(number);

  while (i < limit)
  {
    if((number % i) == 0)
    {
      printf("%llu ",i);
    }
    i = GetNextPrime();
  }

  // printf("Time elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}