/*
IFBhbmRpZ2l0YWwgbXVsdGlwbGVzCgogVGFrZSB0aGUgbnVtYmVyIDE5MiBhbmQg
bXVsdGlwbHkgaXQgYnkgZWFjaCBvZiAxLCAyLCBhbmQgMzoKCiBCeSBjb25jYXRl
bmF0aW5nIGVhY2ggcHJvZHVjdCB3ZSBnZXQgdGhlIDEgdG8gOSBwYW5kaWdpdGFs
LCAxOTIzODQ1NzYuIFdlIHdpbGwgY2FsbCAxOTIzODQ1NzYgdGhlIGNvbmNhdGVu
YXRlZCBwcm9kdWN0IG9mIDE5MiBhbmQgKDEsMiwzKQoKIFRoZSBzYW1lIGNhbiBi
ZSBhY2hpZXZlZCBieSBzdGFydGluZyB3aXRoIDkgYW5kIG11bHRpcGx5aW5nIGJ5
IDEsIDIsIDMsIDQsIGFuZCA1LCBnaXZpbmcgdGhlIHBhbmRpZ2l0YWwsIDkxODI3
MzY0NSwgd2hpY2ggaXMgdGhlIGNvbmNhdGVuYXRlZCBwcm9kdWN0IG9mIDkgYW5k
ICgxLDIsMyw0LDUpLgoKIFdoYXQgaXMgdGhlIGxhcmdlc3QgMSB0byA5IHBhbmRp
Z2l0YWwgOS1kaWdpdCBudW1iZXIgdGhhdCBjYW4gYmUgZm9ybWVkIGFzIHRoZSBj
b25jYXRlbmF0ZWQgcHJvZHVjdCBvZiBhbiBpbnRlZ2VyIHdpdGggKDEsMiwgLi4u
ICwgIG4gKSB3aGVyZSBuICA+IDE/
*/

#include <stdint.h>
#include "../EulerLib/euler.h"
#include <stdio.h>

int main()
{
  unsigned int i,j;
  unsigned int answer = 0;
  unsigned long long int term;

  for (i = 2; i < 200000; i++)
  {
    j = 1;
    term = (i * j);

    while (HowManyDigits(term) < 9)
    {
      j++;
      term = ConcatNumber(term,(i * j));
    }

    if((HowManyDigits(term) == 9) && (isPanDigital(term)))
    {
      //printf("%u %u %u\n",i, j, term);
      if(term > answer)
      {
        answer = term;
      }
    }
  }

  printf("%u\n",answer);

  return 0;
}


