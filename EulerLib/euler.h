/*===============================================================================================================================

  Project Name         : Project Euler

  Software Package     : Euler Lib
           Sub-Package : None
           Owner       : Gerhard van Niekerk

  Abstract             : Euler Lib is a library that has functions that make solving Project Euler Problems easier.

=================================================================================================================================*/

/*--[ Visibility ]---------------------------------------------------------------------------------------------------------------*/
#ifndef _EULER_H_
#define _EULER_H_

/*==[ GLOBALLY VISIBLE ]=========================================================================================================*/
  /*--[ Headers ]---------------------------------------------------------------------------------------------------------------*/
#include <stdint.h>  

  /*--[ Literals ]---------------------------------------------------------------------------------------------------------------*/
#define TWENTY_DIGIT_NUMBER   10000000000000000000llu   /*!< the first decimal number containing 20 digits */
#define LARGEST_FACTORIAL     20                        /*!< largest integer for which an 64bit integer factorial con be computed */

  /*--[ Functions ]---------------------------------------------------------------------------------------------------------------*/

/* From Prime.c */
int32_t isPrime(uint64_t n);
int32_t isComposite (uint64_t n);
uint64_t GetNextPrime(void);
uint64_t ToNextPrime(uint64_t n);

/* From Digits.c */
uint32_t HowManyDigits(uint64_t n);
uint32_t GetDigit(uint64_t n, uint32_t digit);
uint32_t ReplaceDigit(uint32_t num, uint32_t digit, uint32_t rep);
uint32_t ReplaceDigits(uint32_t num, uint32_t digit, uint32_t rep, uint32_t x);
int32_t isPanDigital(uint64_t n);
int32_t isZeroPanDigital(uint64_t n);
uint64_t ConcatThreeNumber(uint32_t n,uint32_t m,uint32_t p);
uint64_t ConcatNumber(uint32_t m,uint32_t p);
uint64_t RightTruncate(uint64_t n, uint32_t digits);
uint64_t LeftTruncate(uint64_t n, uint32_t digits);
int32_t IsPalindrome(uint64_t n);
uint64_t RotateNumber(uint64_t n, int32_t i);
uint32_t ContainsDigit(uint64_t n, uint32_t digit);
uint64_t RemoveDigit(uint64_t n, uint32_t digit);

/* from Math.c*/
uint64_t factorial( int32_t n );
uint32_t FactorCount(uint64_t f);
uint32_t PrimeFactorise(uint32_t number, uint32_t *factors);
uint32_t Factorise(uint32_t number, uint32_t *factors);
int32_t isTriangleNumber(uint64_t t);
int32_t isPentagonalNumber(uint64_t t);
uint64_t  TriangleNumber(uint64_t n);
uint64_t PentagonalNumber(uint64_t n);
uint64_t HexagonalNumber(uint64_t n);
uint32_t isPerfectNumber(uint32_t n);
uint32_t isDeficientNumber(uint32_t n);
uint32_t isAbundantNumber(uint32_t n);
uint32_t isOddNumber(uint64_t n);

/* from tracking.c */
int32_t PreviouslyEncountered(uint32_t n);
void ResetPreviouslyEncountered(void);
int32_t CountPreviouslyEncountered(void);
int32_t TrackMax(uint32_t n);
void ResetTrackMax(void);


/*==[ PACKAGE VISIBLE ]==========================================================================================================*/
#ifdef EULER_LIB_PACKAGE_VISIBLE

#endif

#endif /* _EULER_H_ */
/*===============================================================================================================================*/

