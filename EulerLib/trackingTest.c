#include "tracking.c"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <assert.h>

int main()
{
  // clock_t start = clock();
  int32_t i;


  /* Fill with 100 elements */
  for(i = 0; i < NUMBERTRACKERMAX; i++)
  {
    assert(PreviouslyEncountered(i) == 0);
  }

  /* Test that all 100 elements are present */
  for(i = 0; i < NUMBERTRACKERMAX; i++)
  {
    assert(PreviouslyEncountered(i) == 1);
  }

  /* There should be no space available */
  assert(PreviouslyEncountered(103457) == NUMBERTRACKERMAX);


  /* containing allot */
  assert(CountPreviouslyEncountered() == NUMBERTRACKERMAX);

  /* Start again */
  ResetPreviouslyEncountered();

  /* containing nothing*/
  assert(CountPreviouslyEncountered() == 0);

  /* We should be able to add that same hundered elements from earlier */
  for(i = 0; i < NUMBERTRACKERMAX; i++)
  {
    assert(PreviouslyEncountered(i) == 0);
  }

  for(i = 0; i < NUMBERTRACKERMAX; i++)
  {
    assert(PreviouslyEncountered(i) == 1);
  }

  ResetPreviouslyEncountered();

  assert(TrackMax(0) == 0);
  assert(TrackMax(5) == 5);
  assert(TrackMax(4) == 5);
  assert(TrackMax(6) == 6);
  assert(TrackMax(1235454) == 1235454);
  assert(TrackMax(123545) == 1235454);
  ResetTrackMax();
  assert(TrackMax(5) == 5);

  // printf("Time elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}
