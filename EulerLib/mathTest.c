#include "math.c"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <assert.h>

int main()
{
  // clock_t start = clock();
  uint32_t factors[32];

  uint32_t TriangleNumbers[10] = {1, 3, 6, 10, 15, 21, 28, 36, 45, 55};
  uint32_t NotTriangleNumbers[10] = {2, 4, 5, 7, 8, 9, 11, 12, 13, 14};

  uint32_t PentagonalNumbers[26] = {1, 5, 12, 22, 35, 51, 70, 92, 117, 145, 176, 210, 247, 287, 330, 376, 425, 477, 532, 590, 651, 715, 782, 852, 925, 1001};
  uint32_t NotPentagonalNumbers[26] = {2, 3, 14, 20, 36, 52, 71, 91, 118, 144, 175, 211, 248, 288, 331, 375, 424, 478, 533, 591, 650, 716, 781, 851, 926, 1000};

  uint32_t HexagonalNumbers[22] = {1, 6, 15, 28, 45, 66, 91, 120, 153, 190, 231, 276, 325, 378, 435, 496, 561, 630, 703, 780, 861, 946};

  uint32_t DeficientNumbers[19] = {1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 19, 21, 22, 23};

  int32_t i;

  assert(factorial(1) == 1);
  assert(factorial(2) == 2);
  assert(factorial(4) == 24);
  assert(factorial(10) == 3628800ull);
  assert(factorial(12) == 479001600ull);
  assert(factorial(13) == 6227020800ull);
  assert(factorial(14) == 87178291200ull);
  assert(factorial(16) == 20922789888000ull);
  assert(factorial(20) == 2432902008176640000ull);
  assert(factorial(21) == 0);
  assert(factorial(0) == 1);

  assert(FactorCount(1) == 2);
  assert(FactorCount(2) == 2);
  assert(FactorCount(3) == 2);
  assert(FactorCount(4) == 4);

  assert(PrimeFactorise(1, &factors[0]) == 0);
  assert(PrimeFactorise(2, &factors[0]) == 1);
  assert(PrimeFactorise(3, &factors[0]) == 1);
  assert(PrimeFactorise(4, &factors[0]) == 2);

  assert(Factorise(1, &factors[0]) == 0);
  assert(Factorise(2, &factors[0]) == 1);
  assert(Factorise(3, &factors[0]) == 1);
  assert(Factorise(4, &factors[0]) == 2);
  assert(Factorise(8, &factors[0]) == 3);
  assert(Factorise(9, &factors[0]) == 2);
  assert(Factorise(10, &factors[0]) == 3);
  assert(Factorise(16, &factors[0]) == 4);


  for(i = 0; i < 10; i++)
  {
    assert(isTriangleNumber(TriangleNumbers[i]) == 1);
  }
  for(i = 0; i < 10; i++)
  {
    assert(isTriangleNumber(NotTriangleNumbers[i]) == 0);
  }

  for(i = 0; i < 10; i++)
  {
    assert(TriangleNumber(i + 1) == TriangleNumbers[i]);
  }


  for(i = 0; i < 26; i++)
  {
    assert(isPentagonalNumber(PentagonalNumbers[i]) == 1);
  }
  for(i = 0; i < 26; i++)
  {
    assert(isPentagonalNumber(NotPentagonalNumbers[i]) == 0);
  }

  for(i = 0; i < 26; i++)
  {
    assert(PentagonalNumber(i + 1) == PentagonalNumbers[i]);
  }

  for(i = 0; i < 21; i++)
  {
    assert(HexagonalNumber(i + 1) == HexagonalNumbers[i]);
  }


  assert(isPerfectNumber(6)  == 1);
  assert(isPerfectNumber(28) == 1);
  assert(isPerfectNumber(496) == 1);
  assert(isPerfectNumber(8128) == 1);
  assert(isPerfectNumber(828) == 0);
  for(i = 0; i < 19; i++)
  {
    assert(isPerfectNumber(DeficientNumbers[i]) == 0);
  }


  for(i = 0; i < 19; i++)
  {
    assert(isDeficientNumber(DeficientNumbers[i]) == 1);
  }
  assert(isDeficientNumber(6) == 0);
  assert(isDeficientNumber(8128) == 0);

  for(i = 0; i < 19; i++)
  {
    assert(isAbundantNumber(DeficientNumbers[i]) == 0);
  }
  assert(isAbundantNumber(6) == 0);
  assert(isAbundantNumber(8128) == 0);
  assert(isAbundantNumber(12) == 1);


  for (i = 2; i < 10000; i = i + 2)
  {
    assert(isOddNumber(i) == 0);
  }
  for (i = 1; i < 10000; i = i + 2)
  {
    assert(isOddNumber(i) == 1);
  }

  // printf("Time elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}

