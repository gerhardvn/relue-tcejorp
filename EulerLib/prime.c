/*=================================================================================================================================

  Project Name         : Project Euler

  Software Package     : Euler Lib
           Sub-Package : Prime
           Owner       : gerhardvn

  Abstract             : This module contains the function related to primes.

 ================================================================================================================================*/
/*--[ Visibility ]---------------------------------------------------------------------------------------------------------------*/
#define EULER_LIB_PACKAGE_VISIBLE 1

/*--[ Include Files ]------------------------------------------------------------------------------------------------------------*/
#include <math.h>
#include <stdint.h>
#include "euler.h"


/*==[ PUBLIC FUNCTIONS ]=========================================================================================================*/
/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function check if the input number is a prime number.

  Input 0 to 4 handled by ifs.
  Input 5 and higher loop up to square root input time and test division.

  \param n An integer argument to test for Prime.

  \retval  1 Input is Prime
  \retval  0 Input is Not Prime

*/
int32_t isPrime (uint64_t n)
{
  uint64_t i;
  uint64_t limit = ((uint64_t)sqrt(n)) + 1;

  if(n <= 1) /* numbers before any primes */
  {
    return 0;
  }

  /* this will not be cover by the limit so they are tested seperatly and
     the unrolled loop add speed on the most common eliminations */
  if((n % 2) == 0)
  {
    if(n == 2)
    {
      return 1;
    }
    return 0;
  }

  if((n % 3) == 0)
  {
    if(n == 3)
    {
      return 1;
    }
    return 0;
  }

  /* check if divisible by any odd number upto the square root of number*/
  for(i = 5; i < limit; i += 2 )
  {
    if((n % i) == 0)
    {
      return 0;
    }
  }

  return 1;
} 


/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function check if the input number is a Composite number.

  inverting wrapper for isPrime

  \param n An integer argument to test for Composite.

  \retval  1 Input is Composite
  \retval  0 Input is Not Composite

*/
int32_t isComposite (uint64_t n)
{
  if(isPrime(n) == 1)
  {
    return 0;
  }

  return 1;
} 


/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Return the next prime number.

  Checks if the current number is prime. if it is not increment it until a prime is encountered. return that number and increment
  the number to check next time. Use a static to keep track of next number.

  \return  a prime number

*/
uint64_t GetNextPrime(void)
{
  static uint64_t prime = 2; /* the next number to try */

  /* the only even prime */
  if(prime == 2)
  {
    prime++;
    return 2;
  }

  /* Loop until the next prime */
  while(isPrime(prime) == 0)
  {
    prime += 2; /* only check odd numbers to cut down on checks */
  }

  prime += 2; /* new number for next call */

  return (prime - 2);
}


/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Return the next prime number after the input.

  Input 0 to 1 returns 2.
  Input 2 returns 3.
  Input higher than 2 would trigger a seach for a prime under all odd numbers following on n.

  \param n An integer argument to start search for next Prime from .

  \return  A prime number

*/
uint64_t ToNextPrime(uint64_t n)
{
  uint64_t prime = n; /* the next number to try */

  /* the only even prime */
  if(n < 2)
  {
    return 2;
  }

  if(n == 2)
  {
    return 3;
  }

  /* move past prime and make odd */
  if((n % 2) == 0)
  {
    prime++;
  }
  else
  {
    prime += 2;
  }

  /* Loop until the next prime */
  while(isPrime(prime) == 0)
  {
    prime += 2;
  }

  return (prime);
}

/*===============================================================================================================================*/

