/*=================================================================================================================================

  Project Name         : Project Euler

  Software Package     : Euler Lib
           Sub-Package : Digits
           Owner       : gerhardvn

  Abstract             : This module contains the functions to manipulate single digits in a integer number.

 ================================================================================================================================*/
/*--[ Visibility ]---------------------------------------------------------------------------------------------------------------*/
#define EULER_LIB_PACKAGE_VISIBLE 1

/*--[ Include Files ]------------------------------------------------------------------------------------------------------------*/
#include <math.h>
#include <string.h>
#include <stdint.h>
#include "euler.h"

/*==[ PUBLIC FUNCTIONS ]=========================================================================================================*/
/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Count the amount of Digits in a decimal number.

  \param n An integer argument that needs it digits counted.

  \return the digit count.

*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
uint32_t HowManyDigits(uint64_t n)
{
  uint64_t i = 1;
  int32_t digits = 0;

  if(n < (TWENTY_DIGIT_NUMBER))
  {
    do
    {
      i *= 10;
      digits++;
    }while ((n / i) != 0);
  }
  else /* this must be 20. will cause overflow with code above*/
  {
   digits = 20;
  }

  return digits;
} 

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Get the value of a digit in a decimal number

  \param n An integer argument from which a digit is returned.
  \param digit An integer argument indicating which argument to return.
          1=lsd. 

  \return the digit.

*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
uint32_t GetDigit(uint64_t n, uint32_t digit)
{
  uint64_t ret = 0;

  if((digit > 0) && (digit < 20))
  {
    ret = n % (uint64_t)pow(10,digit);
    ret /= (uint64_t) pow(10,(digit - 1));
  }
  else if ((digit == 20) && (n >= TWENTY_DIGIT_NUMBER))
  {
    ret = 1; /* Highest digit of 64bit number is 1*/
  }
  /* else return 0 for invalid input */

  return (uint32_t) ret;
} 

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Delete a digit from a number.

  \param n An integer argument from which a digit is returned.
  \param digit An integer argument indicating which argument to return.

  \return the digit.

*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
uint64_t RemoveDigit(uint64_t n, uint32_t digit)
{
  uint64_t ret = 0;

  if((digit > 0) && (digit < 20))
  {
    uint64_t bottom = n % (uint64_t)pow(10,digit-1);
    uint64_t top = n / (uint64_t) pow(10,(digit));
    ret = (top * pow(10,digit-1)) + bottom;
  }

  return ret;
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Get the value of a digit in a decimal number

  \param n An integer argument from which a digit is returned.
  \param digit An integer argument indicating which argument to return.

  \return the digit.

*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
uint32_t ContainsDigit(uint64_t n, uint32_t digit)
{
  uint32_t ret = 21;
  uint32_t d = HowManyDigits(n);

  for (uint32_t r = d; r > 0; r--)
  {
    uint32_t x = GetDigit(n,r);
    if(x == digit)
    {
      ret = r;
      break;
    }
  }

  return ret;
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Replace a digit in a decimal number

  \param num An integer argument from which a digit is replaced.
  \param digit An integer argument indicating which digit.

  \return the digit.

*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
uint32_t ReplaceDigit(uint32_t num, uint32_t digit, uint32_t rep)
{
   uint32_t res = 0;
   uint32_t rem;
   
  if((digit > 0) && (digit < 10))
  {
    if (digit == 1)
    {
      rem = 0;
      res = num - (num % 10);
      res += rep;
    }
    else
    {
      rem = num % (uint32_t)(pow(10, digit -1));
      res = num - rem;
      res -= (GetDigit(res,digit) * (uint32_t)(pow(10, digit -1)));
      res += rem + (rep * (uint32_t)pow(10, digit -1));
    }
  }
  
  return res;
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Replace a digit in a decimal number

  \param num An integer argument from which a digit is replaced.
  \param digit An integer argument indicating which digit.

  \return the digit.

*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
uint32_t ReplaceDigits(uint32_t num, uint32_t digit, uint32_t rep, uint32_t x)
{
  uint32_t res = num;
  
  for(unsigned i = 0; i < x; i++)
  {
    res = ReplaceDigit(res, digit + i, rep);
  }
  
   return res;
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function checks if a number is a Pandigital number [1-9]

  \param n An integer to check.

  \retval  0 Input is not a Pandigital number.
  \retval  1 Input is a Pandigital number.

*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
int32_t isPanDigital(uint64_t n)
{
  int32_t digits = HowManyDigits(n);
  int32_t occured[digits];
  int32_t i;
  int32_t digit;

  if(digits > 9)
  {
    return 0;
  }

  memset((void *)&occured,0,4*digits);

  for(i = 0; i < digits; i++)
  {
    digit = GetDigit(n,i+1);

    if(digit == 0)
    {
      return 0;
    }

    if(digit > digits)
    {
      return 0;
    }

    if(occured[digit - 1])
    {
      return 0;
    }
    else
    {
      occured[digit - 1] = 1;
    }
  }

  return 1;
} 

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function checks if a number is a Pandigital number [0-9]

  \param n An integer to check.

  \retval  0 Input is not a Pandigital number.
  \retval  1 Input is a Pandigital number.

*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
int32_t isZeroPanDigital(uint64_t n)
{
  int32_t digits = HowManyDigits(n);
  int32_t occured[digits + 1];
  int32_t i;
  int32_t digit;

  if(digits > 10)
  {
    return 0;
  }

  memset((void *)&occured,0,((4*digits) + 4));

  for(i = 0; i < digits; i++)
  {
    digit = GetDigit(n,i+1);

    if(digit > digits)
    {
      return 0;
    }

    if(occured[digit])
    {
      return 0;
    }
    else
    {
      occured[digit] = 1;
    }
  }

  for(i = 0; i < digits; i++)
  {
    if(occured[i] != 1)
    {
      return 0;
    }
  }

  return 1;
} 

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Concatenate 3 numbers into one

  \param n Becomes Most sinificant part.
  \param m Becomes middle part.
  \param p Becomes Least significant part.

  \return the concatenated number.

  TODO this can overflow

*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
uint64_t ConcatThreeNumber(uint32_t n,uint32_t m,uint32_t p)
{
  uint64_t ret = p;
  if(m != 0)
  {
    ret += (m * ((uint64_t)pow(10,HowManyDigits(ret))));
    ret += (n * ((uint64_t)pow(10,HowManyDigits(ret))));
  }
  else
  {
    ret += (n * ((uint64_t)pow(10,(HowManyDigits(ret) + 1))));
  }

  return ret;
} 

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Concatenate 2 numbers into one

  \param m Becomes Most sinificant part.
  \param p Becomes Least significant part.

  \return the concatenated number.

*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
uint64_t ConcatNumber(uint32_t m,uint32_t p)
{
  uint64_t ret = p;
  ret += (m * (uint64_t)(pow(10,HowManyDigits(ret))));
  return ret;
} 

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Left Rotate a Number

  \param n To be Rotated.
  \param i by this many digits

  \return the Rotated number.

*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
uint64_t RotateNumber(uint64_t n, int32_t i)
{
  uint64_t DivideFactor = (uint64_t)pow(10,i);
  uint64_t ShiftFactor = ((uint64_t)pow(10,HowManyDigits(n))) / DivideFactor;

  uint64_t returnval;

  if(i == 0)
  {
    return n;
  }

  returnval = (((n % DivideFactor) * ShiftFactor) + (n / DivideFactor));
  return returnval;
} 

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function checks if a number is a Palindromic number

  \param n An integer to check.

  \retval  0 Input is not a Palindromic number.
  \retval  1 Input is a Palindromic number.

*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
int32_t IsPalindrome(uint64_t n)
{
  int32_t i = 0;
  int32_t digits = HowManyDigits(n);

  for (i = 0; i < (digits / 2); i++)
  {
    if(GetDigit(n,i+1) != GetDigit(n,(digits - i)))
    {
      return 0;
    }
  }

  return 1;
} 

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Truncate the Least significant digits

  \param n To be truncated.
  \param digits by these many digits

  \return the Truncated number.

*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
uint64_t RightTruncate(uint64_t n, uint32_t digits)
{
  uint64_t ret = 0;

  if(digits < 20)
  {
    ret = n / (uint64_t)pow(10,digits);
  }
  /* else 20 and more digits overflow 64 bit which can only have 20 digits. Return 0*/

  return  ret;
} 

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Truncate the Most significant digits

  \param n To be truncated.
  \param digits by these many digits

  \return the Truncated number.

*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
uint64_t LeftTruncate(uint64_t n, uint32_t digits)
{
  uint64_t d = (uint64_t)pow(10,(HowManyDigits(n) - digits));
  uint64_t ret;

  if(digits > 0)
  {
    ret = n % d;
  }
  else
  {
    ret = n;
  }

  return  ret;
} 

/*===============================================================================================================================*/
