/*=================================================================================================================================

  Project Name         : Project Euler

  Software Package     : Euler Lib
           Sub-Package : Digits
           Owner       : gerhardvn

  Abstract             : This module contains the functions to manipulate single digits in a integer number.

 ================================================================================================================================*/
/*--[ Visibility ]---------------------------------------------------------------------------------------------------------------*/
#define EULER_LIB_PACKAGE_VISIBLE 1

/*--[ Include Files ]------------------------------------------------------------------------------------------------------------*/
#include <string.h>
#include <stdint.h>
#include "euler.h"

/*--[ Private Literals ]---------------------------------------------------------------------------------------------------------*/
#define NUMBERTRACKERMAX 1000                  /*!< The maximum number of numbers that can be tracked */

/*--[ Private Data ]-------------------------------------------------------------------------------------------------------------*/
static uint32_t collection[NUMBERTRACKERMAX];   /*!< Array of previously encountered integers */
static uint32_t xcount = 0;                     /*!< Place to add next new element */

static uint32_t max = 0;                        /*!< Place to add next new element */


/*==[ PUBLIC FUNCTIONS ]=========================================================================================================*/
/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Check if a number has been encountered previously.

  \param n The integer to check.

  \retval  NUMBERTRACKERMAX No new data can be added
  \retval  1                Input has been found previously
  \retval  0                Input is New

*/
int32_t PreviouslyEncountered(uint32_t n)
{
  uint32_t i;

  /* Search thru all the numbers encountered so far see if there is a match */
  for(i = 0; i < xcount; i++)
  {
    /* If value has been seen before */
    if(n == collection[i])
    {
      return 1;
    }
  }

  /* Add the number to encountered values if there is space */
  if(xcount < NUMBERTRACKERMAX )
  {
    collection[xcount] = n;
    xcount++;
  }
  else
  {
    return NUMBERTRACKERMAX ;
  }

  return 0;

} 

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Reset the data used for tracking in PreviouslyEncountered.
*/
void ResetPreviouslyEncountered(void)
{
  xcount = 0;
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief return the amount of numbers that have been encountered.

  \return The current count

*/
int32_t CountPreviouslyEncountered(void)
{
  return xcount;
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Track the maximum number input

  \param n The integer to check for max.

  \return The current max

*/
int32_t TrackMax(uint32_t n)
{
  if(n > max)
  {
    max = n;
  }
  return max;
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief Reset the data used for tracking in TrackMax.
*/
void ResetTrackMax(void)
{
  max = 0;
}
