/*=================================================================================================================================

  Project Name         : Project Euler

  Software Package     : Euler Lib
           Sub-Package : Math
           Owner       : gerhardvn

  Abstract             : This module contains the mathematical functions.

 ================================================================================================================================*/
/*--[ Visibility ]---------------------------------------------------------------------------------------------------------------*/
#define EULER_LIB_PACKAGE_VISIBLE 1

/*--[ Include Files ]------------------------------------------------------------------------------------------------------------*/
#include <math.h>
#include <stdint.h>
#include "euler.h"

/*==[ PUBLIC FUNCTIONS ]=========================================================================================================*/
/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function computes the factorial of the input number.

  Input upto LARGEST_FACTORIAL will compute a factorial using a loop.
  Input larger than LARGEST_FACTORIAL will result in a number larger than 64 bit and so is not computed.

  \param n An integer for which the factorial must be computed.

  \retval  0 Input is out of range.
  \return    the factotial answer.

*/
uint64_t factorial( int32_t n )
{
  uint64_t fact = 1;

  if(n > LARGEST_FACTORIAL) /* limit check */
  {
    return 0;
  }

  while ( n > 1)  /* Compute */
  {
    fact = fact * n;
    n = n - 1;
  }

  return fact;
} 

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function computes the number of factors existing for the input number.

  \param f An integer for which the factor count must be computed.

  \return    the factor count.

*/
uint32_t FactorCount(uint64_t f)
{
  uint32_t  n = 1;
  uint32_t  sum = 0;
  uint32_t  lim = (uint32_t) sqrt(f);

  while (n <= lim)
  {
    if((f % n) == 0)
    {
      sum += 2;
    }
    n++;
  }

  return sum;
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function computes the number of Prime factors existing for the input number.

  \param number An integer for which the factors must be computed.
  \param *factors returns an array containing the Prime factors of the input.

  \return    the factor count.

*/
uint32_t PrimeFactorise(uint32_t number, uint32_t *factors)
{
  uint32_t  n = 2;
  uint32_t  sum = 0;
  uint32_t  lim = (uint32_t) sqrt(number);

  if(number < 4)
  {
    lim = number;
  }


  while (n <= lim)
  {
    if((number % n) == 0)
    {
      if(isPrime(n))
      {
        factors[sum] = n;
        sum++;
      }
      if(isPrime(number/n))
      {
        factors[sum] = number/n;
        sum++;
      }

    }
    n++;
  }

  return sum;
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function computes the factors existing for the input number.

  \param number An integer for which the factors must be computed.
  \param *factors returns an array containing the factors of the input.

  \return    the factor count.

*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
uint32_t Factorise(uint32_t number, uint32_t *factors)
{
  uint32_t  n = 1;
  uint32_t  sum = 0;
  uint32_t  lim = (uint32_t) sqrt(number);

  if(number == 1)
  {
    return 0;
  }

  if(number < 4)
  {
    factors[0] = 1;
    return 1;
  }


  while (n <= lim)
  {
    if((number % n) == 0)
    {
      factors[sum] = n;
      sum++;

      if((n != 1) && (number/n != n))
      {
        factors[sum] = number/n;
        sum++;
      }
    }
    n++;
  }

  return sum;
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function checks if a number is a triangle number

  \param n An integer to check.

  \retval  0 Input is not a triangle number.
  \retval  1 Input is a triangle number.

*/
int32_t isTriangleNumber(uint64_t t)
{
  uint32_t  n = (uint32_t) sqrt(2 * t);

  if(((n * (n + 1)) / 2) == t)
  {
    return 1;
  }
  return 0;
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function computes the triangle number for the input number.

  \param n An integer for which the triangle number must be computed.

  \return    the triangle number.

*/
uint64_t  TriangleNumber(uint64_t n)
{
  return ((n * (n + 1)) / 2);
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function checks if a number is a Pentagonal number

  \param n An integer to check.

  \retval  0 Input is not a triangle number.
  \retval  1 Input is a Pentagonal number.

*/
int32_t isPentagonalNumber(uint64_t t)
{
  uint32_t  n = (((uint32_t)(sqrt(24 * t)) + 1) + 1)/ 6;

  if( ((n * ((3 * n) -1)) / 2) == t)
  {
    return 1;
  }
  return 0;
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function computes the PentagonalNumber number for the input number.

  \param n An integer for which the PentagonalNumber number must be computed.

  \return    the PentagonalNumber number.

*/
uint64_t PentagonalNumber(uint64_t n)
{
  return ((n * ((3 * n) -1)) / 2);
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function computes the HexagonalNumber number for the input number.

  \param n An integer for which the HexagonalNumber number must be computed.

  \return    the HexagonalNumber number.

*/
uint64_t HexagonalNumber(uint64_t n)
{
  return (n * ((2 * n) -1));
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function checks if a number is a Perfect number

  \param n An integer to check.

  \retval  0 Input is not a Perfect number.
  \retval  1 Input is a Perfect number.

*/
uint32_t isPerfectNumber(uint32_t n)
{
  uint32_t nFactors;
  uint32_t factors[2048];

  uint32_t i;
  uint32_t sum = 0;

  nFactors = Factorise(n, &factors[0]);

  for (i = 0; i < nFactors; i++)
  {
    sum += factors[i];
  }

  if(n == sum)
  {
    return 1;
  }
  return 0;
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function checks if a number is a Deficient number

  \param n An integer to check.

  \retval  0 Input is not a Deficient number.
  \retval  1 Input is a Deficient number.

*/
uint32_t isDeficientNumber(uint32_t n)
{
  uint32_t nFactors;
  uint32_t factors[2048];

  uint32_t i;
  uint32_t sum = 0;

  nFactors = Factorise(n, &factors[0]);

  for (i = 0; i <nFactors; i++)
  {
    sum += factors[i];
  }

  if(n > sum)
  {
    return 1;
  }
  return 0;
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function checks if a number is a Abundant number

  \param n An integer to check.

  \retval  0 Input is not a Abundant number.
  \retval  1 Input is a Abundant number.

*/
uint32_t isAbundantNumber(uint32_t n)
{
  uint32_t nFactors;
  uint32_t factors[2048];

  uint32_t i;
  uint32_t sum = 0;

  nFactors = Factorise(n, &factors[0]);

  for (i = 0; i <nFactors; i++)
  {
    sum += factors[i];
  }

  if(n < sum)
  {
    return 1;
  }
  return 0;
}

/*--[ Function ]-----------------------------------------------------------------------------------------------------------------*/
/*!
  \brief This function checks the input number to be odd.

  \param n An integer to Check.

  \retval  0 Input is not a Odd number.
  \retval  1 Input is a Odd number.

*/
uint32_t isOddNumber(uint64_t n)
{
  return (n%2?1:0);
} 

/*===============================================================================================================================*/
