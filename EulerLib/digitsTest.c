#include "digits.c"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <assert.h>

int main()
{
  // clock_t start = clock();

  /* Test HowManyDigits */
  assert(HowManyDigits(0) == 1);
  assert(HowManyDigits(2) == 1);
  assert(HowManyDigits(9) == 1);
  assert(HowManyDigits(10) == 2);
  assert(HowManyDigits(25) == 2);
  assert(HowManyDigits(176) == 3);
  assert(HowManyDigits(7698) == 4);
  assert(HowManyDigits(53432) == 5);
  assert(HowManyDigits(234354) == 6);
  assert(HowManyDigits(6985634) == 7);
  assert(HowManyDigits(21548745) == 8);
  assert(HowManyDigits(765432423) == 9);

  assert(HowManyDigits(4294967295u) == 10);
  assert(HowManyDigits(18446744073709551615llu) == 20);


  /* Test GetDigit */
  assert(GetDigit(0,0) == 0);
  assert(GetDigit(0,1) == 0);
  assert(GetDigit(0,10) == 0);
  assert(GetDigit(1,0) == 0);
  assert(GetDigit(1,1) == 1);
  assert(GetDigit(1,10) == 0);

  assert(GetDigit(123456789,1) == 9);
  assert(GetDigit(123456789,2) == 8);
  assert(GetDigit(123456789,3) == 7);
  assert(GetDigit(123456789,4) == 6);
  assert(GetDigit(123456789,5) == 5);
  assert(GetDigit(123456789,6) == 4);
  assert(GetDigit(123456789,7) == 3);
  assert(GetDigit(123456789,8) == 2);
  assert(GetDigit(123456789,9) == 1);

  assert(GetDigit(18446744073709551615llu, 18) == 4);
  assert(GetDigit(18446744073709551615llu, 19) == 8);
  assert(GetDigit(18446744073709551615llu, 20) == 1);

  /* RemoveDigit */
  assert(RemoveDigit(123456789,1) == 12345678);
  assert(RemoveDigit(123456789,3) == 12345689);
  assert(RemoveDigit(123456789,5) == 12346789);
  assert(RemoveDigit(123456789,4) == 12345789);

  assert(ContainsDigit(123456789,1) == 9);
  assert(ContainsDigit(123456789,2) == 8);
  assert(ContainsDigit(123456789,8) == 2);
  assert(ContainsDigit(123456789,9) == 1);
  assert(ContainsDigit(12345678,9)  == 21);

  /* ReplaceDigit */
  assert(ReplaceDigit(22,1,3) == 23);
  assert(ReplaceDigit(11,2,3) == 31);
  assert(ReplaceDigit(5555,3,3) == 5355);

  assert(ReplaceDigit(11,1,0) == 10);
  assert(ReplaceDigit(11,1,1) == 11);
  assert(ReplaceDigit(11,1,2) == 12);
  assert(ReplaceDigit(11,1,3) == 13);
  assert(ReplaceDigit(11,1,4) == 14);
  
  assert(ReplaceDigit(123456,3,3) == 123356);

  /* ReplaceDigit */
  assert(ReplaceDigits(22,1,3,1) == 23);
  assert(ReplaceDigits(11,2,3,1) == 31);
  assert(ReplaceDigits(5555,3,3,1) == 5355);
  assert(ReplaceDigits(5555,3,3,2) == 3355);

  /* Test isPanDigital */
  assert(isPanDigital(123456789) == 1);
  assert(isPanDigital(1234567890) == 0);
  assert(isPanDigital(12) == 1);
  assert(isPanDigital(21) == 1);
  assert(isPanDigital(1) == 1);
  assert(isPanDigital(122) == 0);
  assert(isPanDigital(123) == 1);
  assert(isPanDigital(1237654) == 1);
  assert(isPanDigital(124) == 0);
  assert(isPanDigital(0) == 0);


  /* Test isZeroPanDigital */
  assert(isZeroPanDigital(123456789) == 0);
  assert(isZeroPanDigital(1234567890) == 1);
  assert(isZeroPanDigital(12345678901llu) == 0);
  assert(isZeroPanDigital(12) == 0);
  assert(isZeroPanDigital(10) == 1);
  assert(isZeroPanDigital(1) == 0);
  assert(isZeroPanDigital(91) == 0);
  assert(isZeroPanDigital(11) == 0);


  assert(ConcatThreeNumber(1,1,1) == 111);
  assert(ConcatThreeNumber(1,1,0) == 110);
  assert(ConcatThreeNumber(1,0,1) == 101);
  assert(ConcatThreeNumber(0,1,1) == 11);
  assert(ConcatThreeNumber(10,11,12) == 101112);


  assert(ConcatNumber(1,1) == 11);
  assert(ConcatNumber(1,0) == 10);
  assert(ConcatNumber(0,1) == 1);
  assert(ConcatNumber(1234,5678) == 12345678);


  assert(LeftTruncate(123456789,1) == 23456789);
  assert(LeftTruncate(123456789,2) == 3456789);
  assert(LeftTruncate(123456789,3) == 456789);
  assert(LeftTruncate(123456789,4) == 56789);
  assert(LeftTruncate(123456789,5) == 6789);
  assert(LeftTruncate(123456789,6) == 789);
  assert(LeftTruncate(123456789,7) == 89);
  assert(LeftTruncate(123456789,8) == 9);
  assert(LeftTruncate(123456789,9) == 0);

  assert(LeftTruncate(18446744073709551615llu, 0) == 18446744073709551615llu);
  assert(LeftTruncate(18446744073709551615llu, 1) == 8446744073709551615llu);
  assert(LeftTruncate(18446744073709551615llu, 18) == 15);
  assert(LeftTruncate(18446744073709551615llu, 19) == 5);
  assert(LeftTruncate(18446744073709551615llu, 20) == 0);


  assert(RightTruncate(123456789,1) == 12345678);
  assert(RightTruncate(123456789,2) == 1234567);
  assert(RightTruncate(123456789,3) == 123456);
  assert(RightTruncate(123456789,4) == 12345);
  assert(RightTruncate(123456789,5) == 1234);
  assert(RightTruncate(123456789,6) == 123);
  assert(RightTruncate(123456789,7) == 12);
  assert(RightTruncate(123456789,8) == 1);
  assert(RightTruncate(123456789,9) == 0);

  assert(RightTruncate(18446744073709551615llu, 0) == 18446744073709551615llu);
  assert(RightTruncate(18446744073709551615llu, 1) == 1844674407370955161llu);
  assert(RightTruncate(18446744073709551615llu, 18) == 18llu);
  assert(RightTruncate(18446744073709551615llu, 19) == 1llu);
  assert(RightTruncate(18446744073709551615llu, 20) == 0);

  assert(IsPalindrome(121) == 1);
  assert(IsPalindrome(1221) == 1);
  assert(IsPalindrome(12321) == 1);
  assert(IsPalindrome(123321) == 1);
  assert(IsPalindrome(1234321) == 1);
  assert(IsPalindrome(12344321) == 1);
  assert(IsPalindrome(12345678900987654321llu) == 1);

  assert(IsPalindrome(1) == 1);
  assert(IsPalindrome(0) == 1);

  assert(IsPalindrome(1234) == 0);
  assert(IsPalindrome(12344322) == 0);
  assert(IsPalindrome(12345678901987654321llu) == 0);
  assert(IsPalindrome(12345678901987654321llu) == 0);

  assert(RotateNumber(11111, 1) == 11111);
  assert(RotateNumber(12345, 1) == 51234);
  assert(RotateNumber(12345, 0) == 12345);
  assert(RotateNumber(184467440737095516llu, 1) == 618446744073709551llu);
  // add one that cause a roll over

  // printf("Time elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}
