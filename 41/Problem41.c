/*
IFBhbmRpZ2l0YWwgcHJpbWUKCiBXZSBzaGFsbCBzYXkgdGhhdCBhbiBuIC1kaWdp
dCBudW1iZXIgaXMgcGFuZGlnaXRhbCBpZiBpdCBtYWtlcyB1c2Ugb2YgYWxsIHRo
ZSBkaWdpdHMgMSB0byBuCiBleGFjdGx5IG9uY2UuIEZvciBleGFtcGxlLCAyMTQz
IGlzIGEgNC1kaWdpdCBwYW5kaWdpdGFsIGFuZCBpcyBhbHNvIHByaW1lLgoKIFdo
YXQgaXMgdGhlIGxhcmdlc3Qgbi1kaWdpdCBwYW5kaWdpdGFsIHByaW1lIHRoYXQg
ZXhpc3RzPw==
*/

#include <stdint.h>
#include "../EulerLib/euler.h"
#include <stdio.h>
#include <math.h>

int main()
{
  unsigned i = 7654321;
  // sum of digits must not be divisible by 3.
  // Start at biggest canidate and work backwards

  do
  {
    if(isPrime(i))
    {
      if(isPanDigital(i))
      {
        printf("%u\n",i);
        break;
      }
    }
    i -= 2;

  }while(i > 9);

  return 0;
}