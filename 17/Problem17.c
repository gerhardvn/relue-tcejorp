#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <inttypes.h>

char buffy[1024]; 

int OnesDigit(unsigned int i)
{
  int ret  = 0;
  switch (i)
  {
    case 0 :  break;
    case 1 :  ret = sprintf(buffy,"one");
              break;
    case 2 :  ret = sprintf(buffy,"two");
              break;
    case 3 :  ret = sprintf(buffy,"three");
              break;
    case 4 :  ret = sprintf(buffy,"four");
              break;
    case 5 :  ret = sprintf(buffy,"five");
              break;
    case 6 :  ret = sprintf(buffy,"six");
              break;
    case 7 :  ret = sprintf(buffy,"seven");
              break;
    case 8 :  ret = sprintf(buffy,"eight");
              break;
    case 9 :  ret = sprintf(buffy,"nine");
              break;
    default : printf("[[Error]]");
  }
  return ret;
}

int TenersDigit(unsigned int i)
{
  int ret = 0;
  switch (i)
  {
    case 11 : ret = sprintf(buffy,"eleven");
              break;
    case 12 : ret = sprintf(buffy,"twelve");
              break;
    case 13 : ret = sprintf(buffy,"thirteen");
              break;
    case 14 : ret = sprintf(buffy,"fourteen");
              break;
    case 15 : ret = sprintf(buffy,"fifteen");
              break;
    case 16 : ret = sprintf(buffy,"sixteen");
              break;
    case 17 : ret = sprintf(buffy,"seventeen");
              break;
    case 18 : ret = sprintf(buffy,"eighteen");
              break;
    case 19 : ret = sprintf(buffy,"nineteen");
              break;
    default : printf("[[Error]]");
  }
  return ret;
}


int TensDigit(unsigned int i)
{
  int ret = 0;
  switch (i)
  {
    case 0 :  break;
    case 1 :  ret = sprintf(buffy,"ten");
              break;
    case 2 :  ret = sprintf(buffy,"twenty");
              break;
    case 3 :  ret = sprintf(buffy,"thirty");
              break;
    case 4 :  ret = sprintf(buffy,"forty");
              break;
    case 5 :  ret = sprintf(buffy,"fifty");
              break;
    case 6 :  ret = sprintf(buffy,"sixty");
              break;
    case 7 :  ret = sprintf(buffy,"seventy");
              break;
    case 8 :  ret = sprintf(buffy,"eighty");
              break;
    case 9 :  ret = sprintf(buffy,"ninety");
              break;
    default : printf("[[Error]]");
  }
  return ret;
}

int NumberToString(unsigned int i)
{
  int ret = 0;
  if(i > 99)
  {
    ret += OnesDigit(i/100);
    ret += sprintf(buffy," hundred") -1;
    if((i % 100) != 0)
    {
      ret += sprintf(buffy," and ") -2;
    }
    i %= 100;
  }


  if((i > 19) || (i < 11))
  {
    ret += TensDigit(i/10);
    if(((i % 10) != 0) && (i > 10))
    {
      //printf("-");
    }
    ret += OnesDigit(i%10);
  }
  else
  {
    ret += TenersDigit(i);
  }
  return ret;
}

int main()
{
  // clock_t start = clock();
  unsigned int i = 0;
  uint64_t count = 0;
  
  while(i < 999)
  {
    i++;
    count += NumberToString(i);
    //printf(" %"PRIu64"\n",count);
  }

  count += sprintf(buffy,"one thousand") -1;

  printf("%"PRIu64"\n",count);  
  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}
