/*
VHJpYW5nbGUsIHBlbnRhZ29uYWwsIGFuZCBoZXhhZ29uYWwgbnVtYmVycyBhcmUg
Z2VuZXJhdGVkIGJ5IHRoZSBmb2xsb3dpbmcgZm9ybXVsYWU6DQpUcmlhbmdsZSAg
ICAgIFRfKG4pPW4obisxKS8yICAgICAgMSwgMywgNiwgMTAsIDE1LCAuLi4NClBl
bnRhZ29uYWwgICAgICBQXyhuKT1uKDNuLTEpLzIgICAgICAgMSwgNSwgMTIsIDIy
LCAzNSwgLi4uDQpIZXhhZ29uYWwgICAgICAgSF8obik9bigybi0xKSAgICAgICAx
LCA2LCAxNSwgMjgsIDQ1LCAuLi4NCg0KSXQgY2FuIGJlIHZlcmlmaWVkIHRoYXQg
VF8oMjg1KSA9IFBfKDE2NSkgPSBIXygxNDMpID0gNDA3NTUuDQoNCkZpbmQgdGhl
IG5leHQgdHJpYW5nbGUgbnVtYmVyIHRoYXQgaXMgYWxzbyBwZW50YWdvbmFsIGFu
ZCBoZXhhZ29uYWwu
*/

#include "../EulerLib/euler.h"
#include <stdio.h>

int main()
{
  unsigned i;
  unsigned Pi;
  unsigned answer =0;
   
  for(i = 2; i < 100000; i++)
  {
    Pi = HexagonalNumber(i);
    if(isPentagonalNumber(Pi) && isTriangleNumber(Pi))
    {
      //printf("%u(%u)\n", i, Pi);
      if (Pi > answer)
      {
        answer = Pi;
      }
    }
  }
  printf("%u\n", answer);
  return 0;
}