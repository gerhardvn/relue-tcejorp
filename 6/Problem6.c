/*
VGhlIHN1bSBvZiB0aGUgc3F1YXJlcyBvZiB0aGUgZmlyc3QgdGVuIG5hdHVyYWwg
bnVtYmVycyBpcywNCjFeKDIpICsgMl4oMikgKyAuLi4gKyAxMF4oMikgPSAzODUN
ClRoZSBzcXVhcmUgb2YgdGhlIHN1bSBvZiB0aGUgZmlyc3QgdGVuIG5hdHVyYWwg
bnVtYmVycyBpcywNCigxICsgMiArIC4uLiArIDEwKV4oMikgPSA1NV4oMikgPSAz
MDI1DQpIZW5jZSB0aGUgZGlmZmVyZW5jZSBiZXR3ZWVuIHRoZSBzdW0gb2YgdGhl
IHNxdWFyZXMgb2YgdGhlIGZpcnN0IHRlbiBuYXR1cmFsDQpudW1iZXJzIGFuZCB0
aGUgc3F1YXJlIG9mIHRoZSBzdW0gaXMgMzAyNSAtIDM4NSA9IDI2NDAuDQpGaW5k
IHRoZSBkaWZmZXJlbmNlIGJldHdlZW4gdGhlIHN1bSBvZiB0aGUgc3F1YXJlcyBv
ZiB0aGUgZmlyc3Qgb25lIGh1bmRyZWQNCm5hdHVyYWwgbnVtYmVycyBhbmQgdGhl
IHNxdWFyZSBvZiB0aGUgc3VtLg0K
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

unsigned int SquareSum(unsigned int max)
{
  unsigned int i = 0;
  unsigned int r = 0;

  while (i < max)
  {
    i++;
    r += i * i;
  }

  return r;
}

unsigned int SumSquare(unsigned int max)
{
  unsigned int i = 0;
  unsigned int r = 0;

  while (i < max)
  {
    i++;
    r += i;
  }

  return (r*r);
}


int main()
{
  // clock_t start = clock();

  printf ("%u\n",(SumSquare(100) - SquareSum(100)));

  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}