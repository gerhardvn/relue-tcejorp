/*
QSBwZXJmZWN0IG51bWJlciBpcyBhIG51bWJlciBmb3Igd2hpY2ggdGhlIHN1bSBv
ZiBpdHMgcHJvcGVyIGRpdmlzb3JzIGlzDQpleGFjdGx5IGVxdWFsIHRvIHRoZSBu
dW1iZXIuIEZvciBleGFtcGxlLCB0aGUgc3VtIG9mIHRoZSBwcm9wZXIgZGl2aXNv
cnMgb2YgMjgNCndvdWxkIGJlIDEgKyAyICsgNCArIDcgKyAxNCA9IDI4LCB3aGlj
aCBtZWFucyB0aGF0IDI4IGlzIGEgcGVyZmVjdCBudW1iZXIuDQoNCkEgbnVtYmVy
IHdob3NlIHByb3BlciBkaXZpc29ycyBhcmUgbGVzcyB0aGFuIHRoZSBudW1iZXIg
aXMgY2FsbGVkIGRlZmljaWVudCBhbmQNCmEgbnVtYmVyIHdob3NlIHByb3BlciBk
aXZpc29ycyBleGNlZWQgdGhlIG51bWJlciBpcyBjYWxsZWQgYWJ1bmRhbnQuDQoN
CkFzIDEyIGlzIHRoZSBzbWFsbGVzdCBhYnVuZGFudCBudW1iZXIsIDEgKyAyICsg
MyArIDQgKyA2ID0gMTYsIHRoZSBzbWFsbGVzdA0KbnVtYmVyIHRoYXQgY2FuIGJl
IHdyaXR0ZW4gYXMgdGhlIHN1bSBvZiB0d28gYWJ1bmRhbnQgbnVtYmVycyBpcyAy
NC4gQnkNCm1hdGhlbWF0aWNhbCBhbmFseXNpcywgaXQgY2FuIGJlIHNob3duIHRo
YXQgYWxsIGludGVnZXJzIGdyZWF0ZXIgdGhhbiAyODEyMyBjYW4NCmJlIHdyaXR0
ZW4gYXMgdGhlIHN1bSBvZiB0d28gYWJ1bmRhbnQgbnVtYmVycy4gSG93ZXZlciwg
dGhpcyB1cHBlciBsaW1pdCBjYW5ub3QNCmJlIHJlZHVjZWQgYW55IGZ1cnRoZXIg
YnkgYW5hbHlzaXMgZXZlbiB0aG91Z2ggaXQgaXMga25vd24gdGhhdCB0aGUgZ3Jl
YXRlc3QNCm51bWJlciB0aGF0IGNhbm5vdCBiZSBleHByZXNzZWQgYXMgdGhlIHN1
bSBvZiB0d28gYWJ1bmRhbnQgbnVtYmVycyBpcyBsZXNzIHRoYW4NCnRoaXMgbGlt
aXQuDQoNCkZpbmQgdGhlIHN1bSBvZiBhbGwgdGhlIHBvc2l0aXZlIGludGVnZXJz
IHdoaWNoIGNhbm5vdCBiZSB3cml0dGVuIGFzIHRoZSBzdW0gb2YNCnR3byBhYnVu
ZGFudCBudW1iZXJzLg==
*/

#include "../EulerLib/euler.h"
#include <stdio.h>
#include <math.h>

int main()
{
  unsigned int i,j;
  unsigned int answer = 1;
  unsigned int Abundant = 0;

  for (i = 2; i <= 28123; i++)
  {
    for(j = 12; j < i; j++)
    {
      if((isAbundantNumber(j) == 1) && (isAbundantNumber(i - j) == 1))
      {
        Abundant = 1;
        break;
      }
    }

    if(Abundant == 0)
    {
      answer += i;
    }
    Abundant = 0;
  }

  printf("%u\n",answer);

  return 0;
}
