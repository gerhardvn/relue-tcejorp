/*
Qnkgc3RhcnRpbmcgYXQgdGhlIHRvcCBvZiB0aGUgdHJpYW5nbGUgYmVsb3cgYW5k
IG1vdmluZyB0byBhZGphY2VudCBudW1iZXJzIG9uIHRoZSByb3cgYmVsb3csIHRo
ZSBtYXhpbXVtIHRvdGFsIGZyb20gdG9wIHRvIGJvdHRvbSBpcyAyMy4NCjMNCjcg
NQ0KMiA0IDYNCjggNSA5IDMNClRoYXQgaXMsIDMgKyA3ICsgNCArIDkgPSAyMy4N
CkZpbmQgdGhlIG1heGltdW0gdG90YWwgZnJvbSB0b3AgdG8gYm90dG9tIG9mIHRo
ZSB0cmlhbmdsZSBiZWxvdzoNCjc1DQo5NSA2NA0KMTcgNDcgODINCjE4IDM1IDg3
IDEwDQoyMCAwNCA4MiA0NyA2NQ0KMTkgMDEgMjMgNzUgMDMgMzQNCjg4IDAyIDc3
IDczIDA3IDYzIDY3DQo5OSA2NSAwNCAyOCAwNiAxNiA3MCA5Mg0KNDEgNDEgMjYg
NTYgODMgNDAgODAgNzAgMzMNCjQxIDQ4IDcyIDMzIDQ3IDMyIDM3IDE2IDk0IDI5
DQo1MyA3MSA0NCA2NSAyNSA0MyA5MSA1MiA5NyA1MSAxNA0KNzAgMTEgMzMgMjgg
NzcgNzMgMTcgNzggMzkgNjggMTcgNTcNCjkxIDcxIDUyIDM4IDE3IDE0IDkxIDQz
IDU4IDUwIDI3IDI5IDQ4DQo2MyA2NiAwNCA2OCA4OSA1MyA2NyAzMCA3MyAxNiA2
OSA4NyA0MCAzMQ0KMDQgNjIgOTggMjcgMjMgMDkgNzAgOTggNzMgOTMgMzggNTMg
NjAgMDQgMjMNCk5PVEU6IEFzIHRoZXJlIGFyZSBvbmx5IDE2Mzg0IHJvdXRlcywg
aXQgaXMgcG9zc2libGUgdG8gc29sdmUgdGhpcyBwcm9ibGVtIGJ5IHRyeWluZyBl
dmVyeSByb3V0ZS4gSG93ZXZlciwgUHJvYmxlbSA2NywgaXMgdGhlIHNhbWUgY2hh
bGxlbmdlIHdpdGggYSB0cmlhbmdsZSBjb250YWluaW5nIG9uZS1odW5kcmVkIHJv
d3M7IGl0IGNhbm5vdCBiZSBzb2x2ZWQgYnkgYnJ1dGUgZm9yY2UsIGFuZCByZXF1
aXJlcyBhIGNsZXZlciBtZXRob2QhIDtvKQ0K
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

unsigned int LoadFile(FILE *pfile)
{
  unsigned int num = 0;
  char ch;
  unsigned int i = 0;
  int Read = 0;

  do
  {

    ch = fgetc(pfile);

    if((ch >= '0') && (ch <= '9'))
    {
      num *= 10;
      num += (ch - '0');
      i++;
      Read = 1;
    }
    else if(Read)
    {
      ch = EOF;
    }

  } while (ch != EOF);

  return num;
}

int main()
{
  // clock_t start = clock();

  FILE *numfile;
  unsigned int Triangle[TRIANGLE_SIZE][TRIANGLE_SIZE] = {{0}};

  int i,j;

  numfile = fopen("triangle.txt", "r");

  if (numfile != NULL)
  {

    for(i = 0; i < TRIANGLE_SIZE; i++)
    {
      for(j = 0; j <= i; j++)
      {
        Triangle[i][j] =  LoadFile(numfile);
      }
    }

    fclose(numfile);

    for(i = (TRIANGLE_SIZE - 2); i >= 0; i--)
    {
      for(j = 0; j <= (i); j++)
      {
        if(Triangle[i + 1][j] > Triangle[i+1][j+1])
        {
          Triangle[i][j] += Triangle[i+1][j];
        }
        else
        {
          Triangle[i][j] += Triangle[i+1][j+1];
        }
      }
    }

    printf("%u\n",Triangle[0][0]);

  }

  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);
  return 0;
}
