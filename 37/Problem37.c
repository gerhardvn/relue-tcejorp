/*
 Truncatable primes

 The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 379, 37, and 3.

 Find the sum of the only eleven primes that are both truncatable from left to right and right to left.

 NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "../EulerLib/euler.h"


int isTruncatable(unsigned long long int n)
{
  unsigned int digits = HowManyDigits(n);
  unsigned int i;

  for(i = 1; i < digits; i++)
  {
    if(!(isPrime(RightTruncate(n,i))))
    {
      return 0;
    }
    if(!(isPrime(LeftTruncate(n,i))))
    {
      return 0;
    }
  }
  return 1;
}

int main()
{
  //clock_t start = clock();
  unsigned int i = 10;
  unsigned int count = 0;
  unsigned int sum = 0;

  while (count < 11)
  {
    i++;
    if(isPrime(i))
    {
      //printf("%u Prime\n",i);
      if(isTruncatable(i))
      {
        //printf("%u Truncatable\n",i);
        count++;
        sum += i;
      }
    }
  }

  printf("%u\n",sum);
  //printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}