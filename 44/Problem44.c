/*
UGVudGFnb25hbCBudW1iZXJzIGFyZSBnZW5lcmF0ZWQgYnkgdGhlIGZvcm11bGEs
IFBuPW4oM27iiJIxKS8yLiBUaGUgZmlyc3QgdGVuIHBlbnRhZ29uYWwgbnVtYmVy
cyBhcmU6DQoNCjEsIDUsIDEyLCAyMiwgMzUsIDUxLCA3MCwgOTIsIDExNywgMTQ1
LCAuLi4NCg0KSXQgY2FuIGJlIHNlZW4gdGhhdCBQNCArIFA3ID0gMjIgKyA3MCA9
IDkyID0gUDguIEhvd2V2ZXIsIHRoZWlyIGRpZmZlcmVuY2UsIDcwIOKIkiAyMiA9
IDQ4LCBpcyBub3QgcGVudGFnb25hbC4NCg0KRmluZCB0aGUgcGFpciBvZiBwZW50
YWdvbmFsIG51bWJlcnMsIFBqIGFuZCBQaywgZm9yIHdoaWNoIHRoZWlyIHN1bSBh
bmQgZGlmZmVyZW5jZSBhcmUgcGVudGFnb25hbCBhbmQgRCA9IHxQayDiiJIgUGp8
IGlzIG1pbmltaXNlZDsgd2hhdCBpcyB0aGUgdmFsdWUgb2YgRD8=
*/

#include "../EulerLib/euler.h"
#include <stdio.h>

int main()
{
  unsigned int i,j;
  unsigned int Pi,Pj;

  for(i = 1; i < 10000; i++)
  {
    Pi = PentagonalNumber(i);
    for(j = i + 1; j < 10000; j++)
    {
      Pj = PentagonalNumber(j);
      if(isPentagonalNumber(Pj + Pi) && isPentagonalNumber(Pj - Pi))
      {
        //printf("%u(%u) %u(%u) %u", i, Pi, j, Pj, (Pj - Pi));
        printf("%u\n", (Pj - Pi));
        return 0;
      }
    }
  }
  return 0;
}