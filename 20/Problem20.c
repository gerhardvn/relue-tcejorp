#include <gmp.h>
#include <stdio.h>
#include <inttypes.h>

int main()
{
  uint64_t sum = 0;
  
  mpz_t fac;
  mpz_t temp;
  mpz_init(temp);
  mpz_init(fac);

  mpz_fac_ui(fac, 100);
  //gmp_printf("%Zd\n" , fac);

  while(mpz_cmp_ui(fac,0) != 0)
  {
    sum += mpz_tdiv_q_ui (temp, fac, 10);
    mpz_set(fac,temp);
  }

  printf("%" PRIu64 "\n",sum);

  return 0;
}

