for %%I in (.) do set CurrDirName=%%~nxI

for /f %%i in ("Problem%CurrDirName%.c") do set size=%%~zi
if %size% gtr 0 goto aLabel

echo /* > Problem%CurrDirName%.c
echo. >> Problem%CurrDirName%.c

curl -s https://projecteuler.net/problem=%CurrDirName% | .\..\ci\pup -p "h2" | tail -2 | head -1 >> Problem%CurrDirName%.c
curl -s https://projecteuler.net/problem=%CurrDirName% | .\..\ci\pup -p "p" | sed "/<[^>]*>/d" >> Problem%CurrDirName%.c

echo. >> Problem%CurrDirName%.c
echo */ >> Problem%CurrDirName%.c

:aLabel
start notepad++ Problem%CurrDirName%.c