@echo off
for %%I in (.) do set CurrDirName=%%~nxI
set PrePended=00%CurrDirName%
set ProblemName=%PrePended:~-3%
grep "^Problem %ProblemName%: \(.*\)" ..\Answers.txt | cut -c1-13 --complement
Problem%CurrDirName%.exe

grep "^Problem %ProblemName%: \(.*\)" ..\Answers.txt | cut -c1-13 --complement > temp.txt
set /p Correct=<temp.txt
Problem%CurrDirName%.exe > temp.txt
set /p Answer=<temp.txt

del /Q temp.txt

if %Correct%==%Answer% ( echo Pass ) else ( echo FAIL FAIL FAIL FAIL )