REM For building when using euler lib
for %%I in (.) do set CurrDirName=%%~nxI

cp ../CI/2/go.bat go.bat
cp ../CI/2/.gitlab-ci.yml .gitlab-ci.yml
> Problem%CurrDirName%.c

sed -i 's/Problem/Problem%CurrDirName%/g' go.bat
sed -i 's/Problem/Problem%CurrDirName%/g' .gitlab-ci.yml
sed -i 's/XXX/%CurrDirName%/g' .gitlab-ci.yml

unix2dos go.bat
unix2dos .gitlab-ci.yml

echo   - local: '%CurrDirName%/.gitlab-ci.yml' >> ../.gitlab-ci.yml
