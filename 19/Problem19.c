/*
WW91IGFyZSBnaXZlbiB0aGUgZm9sbG93aW5nIGluZm9ybWF0aW9uLCBidXQgeW91
IG1heSBwcmVmZXIgdG8gZG8gc29tZSByZXNlYXJjaCBmb3IgeW91cnNlbGYuDQoN
CiAgICAqIDEgSmFuIDE5MDAgd2FzIGEgTW9uZGF5Lg0KICAgICogVGhpcnR5IGRh
eXMgaGFzIFNlcHRlbWJlciwNCiAgICAgIEFwcmlsLCBKdW5lIGFuZCBOb3ZlbWJl
ci4NCiAgICAgIEFsbCB0aGUgcmVzdCBoYXZlIHRoaXJ0eS1vbmUsDQogICAgICBT
YXZpbmcgRmVicnVhcnkgYWxvbmUsDQogICAgICBXaGljaCBoYXMgdHdlbnR5LWVp
Z2h0LCByYWluIG9yIHNoaW5lLg0KICAgICAgQW5kIG9uIGxlYXAgeWVhcnMsIHR3
ZW50eS1uaW5lLg0KICAgICogQSBsZWFwIHllYXIgb2NjdXJzIG9uIGFueSB5ZWFy
IGV2ZW5seSBkaXZpc2libGUgYnkgNCwgYnV0IG5vdCBvbiBhIGNlbnR1cnkgdW5s
ZXNzIGl0IGlzIGRpdmlzaWJsZSBieSA0MDAuDQoNCkhvdyBtYW55IFN1bmRheXMg
ZmVsbCBvbiB0aGUgZmlyc3Qgb2YgdGhlIG1vbnRoIGR1cmluZyB0aGUgdHdlbnRp
ZXRoIGNlbnR1cnkgKDEgSmFuIDE5MDEgdG8gMzEgRGVjIDIwMDApPw0K
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int isLeapYear(unsigned int year)
{
  if((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
  {
    return 1; /* leap */
  }
  else
  {
    return 0; /* no leap */
  }
}

unsigned int DaysInMonth(unsigned int month, unsigned int year)
{
  unsigned int days = 0;

  switch(month)
  {
    case  1:  /* jan */
    case  3:  /* march */
    case  5:  /* may */
    case  7:  /* july*/
    case  8:  /* august */
    case 10:  /* october */
    case 12:  days = 31; /* December */
              break;
    case  4:  /* April */
    case  6:  /* June */
    case  9:  /* september */
    case 11:  days = 30; /* November */
              break;
    case  2:  if(isLeapYear(year))
              {
                days = 29;
              }
              else
              {
                days = 28;
              }
              break;
    default : days = 0;
              break;
  }

  return days;
}

int main()
{
  // clock_t start = clock();
  int years,month;

  unsigned int days = 0;
  unsigned int sundays = 0;
  int remainder = 0; /* start on a monday */

  for(years = 1900; years < 2001; years++)
  {
    for(month = 1; month < 13; month++)
    {
      days = remainder + DaysInMonth(month,years);
      if(days % 7 == 6)
      {
        if(years > 1900)
        {
          sundays++;
        }
        //printf("%04u/%02u---------\n",years,month);
        remainder = -1; /* start with monday again */
      }
      else
      {
        remainder = days % 7;
        //printf("%04u/%02u:%u\n",years,month,remainder);
      }
    }

  }

  printf("%u\n",sundays);

  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}