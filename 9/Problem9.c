#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

void PythagoreanTriplet(unsigned int m, unsigned int n, unsigned int * a, unsigned int * b, unsigned int  * c)
{

  *a = 2 * m * n;
  *b = (m * m) - (n * n);
  *c = (m * m) + (n * n);
}

int main()
{
  // clock_t start = clock();
  unsigned int m,n;
  unsigned int a,b,c;

  for(n = 1; n < 50; n++)
  {
    for(m = n + 1; m < 50; m++)
    {

      PythagoreanTriplet(m,n, &a,&b,&c);

      if((a+b+c) == 1000)
      {
        //printf("\n%3u %3u %3u", a, b, c);
        printf("%u\n",(a*b*c));
      }
    }
  }


  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}
