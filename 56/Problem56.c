/*
QSBnb29nb2wgKDEwXigxMDApKSBpcyBhIG1hc3NpdmUgbnVtYmVyOiBvbmUgZm9s
bG93ZWQgYnkgb25lLWh1bmRyZWQgemVyb3M7DQoxMDBeKDEwMCkgaXMgYWxtb3N0
IHVuaW1hZ2luYWJseSBsYXJnZTogb25lIGZvbGxvd2VkIGJ5IHR3by1odW5kcmVk
IHplcm9zLg0KRGVzcGl0ZSB0aGVpciBzaXplLCB0aGUgc3VtIG9mIHRoZSBkaWdp
dHMgaW4gZWFjaCBudW1iZXIgaXMgb25seSAxLg0KDQpDb25zaWRlcmluZyBuYXR1
cmFsIG51bWJlcnMgb2YgdGhlIGZvcm0sIGFeKGIpLCB3aGVyZSBhLCBiIDwgMTAw
LCB3aGF0IGlzIHRoZSBtYXhpbXVtIGRpZ2l0YWwgc3VtPw==
*/

#include <gmp.h>
#include <stdio.h>

int main()
{
  mpz_t answer;
  unsigned a;
  unsigned b;
  unsigned sum;
  unsigned max = 0;

  mpz_init(answer);

  for(a = 99; a > 10; a--)
  {
    for(b = 99; b > 10; b--)
    {
      sum = 0;
      mpz_ui_pow_ui(answer,a,b);

      while (mpz_cmp_ui(answer,0) != 0)
      {
        sum += mpz_fdiv_q_ui(answer, answer, 10);
      }

      if(sum > max)
      {
        max = sum;
      }

    }
  }

  printf("%u\n",max);
  return 0;
}
