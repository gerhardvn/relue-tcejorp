/*
VGhlIG51bWJlciwgMTQwNjM1NzI4OSwgaXMgYSAwIHRvIDkgcGFuZGlnaXRhbCBu
dW1iZXIgYmVjYXVzZSBpdCBpcyBtYWRlIHVwIG9mIGVhY2ggb2YgdGhlIGRpZ2l0
cyAwIHRvIDkgaW4gc29tZSBvcmRlciwgYnV0IGl0IGFsc28gaGFzIGEgcmF0aGVy
IGludGVyZXN0aW5nIHN1Yi1zdHJpbmcgZGl2aXNpYmlsaXR5IHByb3BlcnR5Lg0K
DQpMZXQgZDEgYmUgdGhlIDFzdCBkaWdpdCwgZDIgYmUgdGhlIDJuZCBkaWdpdCwg
YW5kIHNvIG9uLiBJbiB0aGlzIHdheSwgd2Ugbm90ZSB0aGUgZm9sbG93aW5nOg0K
DQpkMmQzZDQ9NDA2IGlzIGRpdmlzaWJsZSBieSAyDQpkM2Q0ZDU9MDYzIGlzIGRp
dmlzaWJsZSBieSAzDQpkNGQ1ZDY9NjM1IGlzIGRpdmlzaWJsZSBieSA1DQpkNWQ2
ZDc9MzU3IGlzIGRpdmlzaWJsZSBieSA3DQpkNmQ3ZDg9NTcyIGlzIGRpdmlzaWJs
ZSBieSAxMQ0KZDdkOGQ5PTcyOCBpcyBkaXZpc2libGUgYnkgMTMNCmQ4ZDlkMTA9
Mjg5IGlzIGRpdmlzaWJsZSBieSAxNw0KDQpGaW5kIHRoZSBzdW0gb2YgYWxsIDAg
dG8gOSBwYW5kaWdpdGFsIG51bWJlcnMgd2l0aCB0aGlzIHByb3BlcnR5Lg==
*/

#include "../EulerLib/euler.h"
#include <stdio.h>

int DivisorTest(unsigned long long int n)
{
  unsigned int i = 1;
  unsigned int div[7] = {2, 3, 5, 7, 11, 13, 17};
  unsigned int result;

  for(i = 0; i < 7; i++)
  {
    result = LeftTruncate(n,i + 1);
    result = RightTruncate(result, 6 - i);

    if((result % div[i] != 0))
    {
      return 0;
    }
  }
  return 1;

}


void GenZeroPanDigitals(void)
{
  int i[10];
  unsigned long long pandigital;
  unsigned long long solutions = 0;

  for(i[0] = 1; i[0] < 10 ; i[0]++)
  {
    for(i[1] = 0; i[1] < 10 ; i[1]++)
    {
      if(i[0] != i[1])
      {
        for(i[2] = 0; i[2] < 10 ; i[2]++)
        {
          if((i[0] != i[2]) && (i[1] != i[2]))
          {
            for(i[3] = 0; i[3] < 10 ; i[3]++)
            {
              if((i[0] != i[3]) && (i[1] != i[3]) && (i[2] != i[3]))
              {
                for(i[4] = 0; i[4] < 10 ; i[4]++)
                {
                  if((i[0] != i[4]) && (i[1] != i[4]) && (i[2] != i[4]) && (i[3] != i[4]))
                  {
                    for(i[5] = 0; i[5] < 10 ; i[5]++)
                    {
                      if((i[0] != i[5]) && (i[1] != i[5]) && (i[2] != i[5]) && (i[3] != i[5]) && (i[4] != i[5]))
                      {
                        for(i[6] = 0; i[6] < 10 ; i[6]++)
                        {
                          if((i[0] != i[6]) && (i[1] != i[6]) && (i[2] != i[6]) && (i[3] != i[6]) && (i[4] != i[6]) && (i[5] != i[6]))
                          {
                            for(i[7] = 0; i[7] < 10 ; i[7]++)
                            {
                              if((i[0] != i[7]) && (i[1] != i[7]) && (i[2] != i[7]) && (i[3] != i[7]) && (i[4] != i[7]) && (i[5] != i[7]) && (i[6] != i[7]))
                              {
                                for(i[8] = 0; i[8] < 10 ; i[8]++)
                                {
                                  if((i[0] != i[8]) && (i[1] != i[8]) && (i[2] != i[8]) && (i[3] != i[8]) && (i[4] != i[8]) && (i[5] != i[8]) && (i[6] != i[8]) && (i[7] != i[8]))
                                  {
                                    for(i[9] = 0; i[9] < 10 ; i[9]++)
                                    {
                                      if((i[0] != i[9]) && (i[1] != i[9]) && (i[2] != i[9]) && (i[3] != i[9]) && (i[4] != i[9]) && (i[5] != i[9]) && (i[6] != i[9]) && (i[7] != i[9]) && (i[8] != i[9]))
                                      {
                                        pandigital = ConcatNumber(ConcatNumber(ConcatNumber(ConcatNumber(ConcatNumber(ConcatNumber(ConcatNumber(ConcatNumber(ConcatNumber(i[0],i[1]),i[2]),i[3]),i[4]),i[5]),i[6]),i[7]),i[8]),i[9]);
                                        //printf("%llu\n",pandigital);
                                        if(DivisorTest(pandigital))
                                        {
                                          solutions += pandigital;
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  printf("%llu\n",solutions);
}


int main()
{
//  if(DivisorTest(1406357289))
//  {
//    puts("Tested ok");
//  }

  GenZeroPanDigitals();

  return 0;
}