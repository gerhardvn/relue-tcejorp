/*
QW4gaXJyYXRpb25hbCBkZWNpbWFsIGZyYWN0aW9uIGlzIGNyZWF0ZWQgYnkgY29u
Y2F0ZW5hdGluZyB0aGUgcG9zaXRpdmUgaW50ZWdlcnM6DQoNCjAuMTIzNDU2Nzg5
MTAxMTEyMTMxNDE1MTYxNzE4MTkyMDIxLi4uDQoNCkl0IGNhbiBiZSBzZWVuIHRo
YXQgdGhlIDEydGggZGlnaXQgb2YgdGhlIGZyYWN0aW9uYWwgcGFydCBpcyAxLg0K
DQpJZiBkbiByZXByZXNlbnRzIHRoZSBudGggZGlnaXQgb2YgdGhlIGZyYWN0aW9u
YWwgcGFydCwgZmluZCB0aGUgdmFsdWUgb2YgdGhlIGZvbGxvd2luZyBleHByZXNz
aW9uLg0KDQpkMSDDlyBkMTAgw5cgZDEwMCDDlyBkMTAwMCDDlyBkMTAwMDAgw5cg
ZDEwMDAwMCDDlyBkMTAwMDAwMA==
*/

#include "../EulerLib/euler.h"
#include <stdio.h>

unsigned make_digit(unsigned digit)
{
  static unsigned current_nr = 1;
  static unsigned current_digit = 0;
  unsigned answer = 0;
  
  while (!answer)
  {
    for(unsigned i = HowManyDigits(current_nr); i > 0; i--)
    {
      unsigned temp = GetDigit(current_nr, i);
      current_digit++;
      if ((current_digit) == digit)
      { 
        answer = temp;
      }
    }      
    current_nr++;
  }
  return answer; 
}
int main()
{
  unsigned answer = 1;

  answer *= make_digit(1);
  answer *= make_digit(10);
  answer *= make_digit(100);
  answer *= make_digit(1000);
  answer *= make_digit(10000);
  answer *= make_digit(100000);
  answer *= make_digit(1000000);
  printf("%u\n",answer);

  return 0;
}
