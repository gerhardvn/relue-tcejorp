/*
SW4gdGhlIDIww5cyMCBncmlkIGJlbG93LCBmb3VyIG51bWJlcnMgYWxvbmcgYSBk
aWFnb25hbCBsaW5lIGhhdmUgYmVlbiBtYXJrZWQgaW4gcmVkLg0KDQowOCAwMiAy
MiA5NyAzOCAxNSAwMCA0MCAwMCA3NSAwNCAwNSAwNyA3OCA1MiAxMiA1MCA3NyA5
MSAwOA0KNDkgNDkgOTkgNDAgMTcgODEgMTggNTcgNjAgODcgMTcgNDAgOTggNDMg
NjkgNDggMDQgNTYgNjIgMDANCjgxIDQ5IDMxIDczIDU1IDc5IDE0IDI5IDkzIDcx
IDQwIDY3IDUzIDg4IDMwIDAzIDQ5IDEzIDM2IDY1DQo1MiA3MCA5NSAyMyAwNCA2
MCAxMSA0MiA2OSAyNCA2OCA1NiAwMSAzMiA1NiA3MSAzNyAwMiAzNiA5MQ0KMjIg
MzEgMTYgNzEgNTEgNjcgNjMgODkgNDEgOTIgMzYgNTQgMjIgNDAgNDAgMjggNjYg
MzMgMTMgODANCjI0IDQ3IDMyIDYwIDk5IDAzIDQ1IDAyIDQ0IDc1IDMzIDUzIDc4
IDM2IDg0IDIwIDM1IDE3IDEyIDUwDQozMiA5OCA4MSAyOCA2NCAyMyA2NyAxMCAy
NiAzOCA0MCA2NyA1OSA1NCA3MCA2NiAxOCAzOCA2NCA3MA0KNjcgMjYgMjAgNjgg
MDIgNjIgMTIgMjAgOTUgNjMgOTQgMzkgNjMgMDggNDAgOTEgNjYgNDkgOTQgMjEN
CjI0IDU1IDU4IDA1IDY2IDczIDk5IDI2IDk3IDE3IDc4IDc4IDk2IDgzIDE0IDg4
IDM0IDg5IDYzIDcyDQoyMSAzNiAyMyAwOSA3NSAwMCA3NiA0NCAyMCA0NSAzNSAx
NCAwMCA2MSAzMyA5NyAzNCAzMSAzMyA5NQ0KNzggMTcgNTMgMjggMjIgNzUgMzEg
NjcgMTUgOTQgMDMgODAgMDQgNjIgMTYgMTQgMDkgNTMgNTYgOTINCjE2IDM5IDA1
IDQyIDk2IDM1IDMxIDQ3IDU1IDU4IDg4IDI0IDAwIDE3IDU0IDI0IDM2IDI5IDg1
IDU3DQo4NiA1NiAwMCA0OCAzNSA3MSA4OSAwNyAwNSA0NCA0NCAzNyA0NCA2MCAy
MSA1OCA1MSA1NCAxNyA1OA0KMTkgODAgODEgNjggMDUgOTQgNDcgNjkgMjggNzMg
OTIgMTMgODYgNTIgMTcgNzcgMDQgODkgNTUgNDANCjA0IDUyIDA4IDgzIDk3IDM1
IDk5IDE2IDA3IDk3IDU3IDMyIDE2IDI2IDI2IDc5IDMzIDI3IDk4IDY2DQo4OCAz
NiA2OCA4NyA1NyA2MiAyMCA3MiAwMyA0NiAzMyA2NyA0NiA1NSAxMiAzMiA2MyA5
MyA1MyA2OQ0KMDQgNDIgMTYgNzMgMzggMjUgMzkgMTEgMjQgOTQgNzIgMTggMDgg
NDYgMjkgMzIgNDAgNjIgNzYgMzYNCjIwIDY5IDM2IDQxIDcyIDMwIDIzIDg4IDM0
IDYyIDk5IDY5IDgyIDY3IDU5IDg1IDc0IDA0IDM2IDE2DQoyMCA3MyAzNSAyOSA3
OCAzMSA5MCAwMSA3NCAzMSA0OSA3MSA0OCA4NiA4MSAxNiAyMyA1NyAwNSA1NA0K
MDEgNzAgNTQgNzEgODMgNTEgNTQgNjkgMTYgOTIgMzMgNDggNjEgNDMgNTIgMDEg
ODkgMTkgNjcgNDgNCg0KVGhlIHByb2R1Y3Qgb2YgdGhlc2UgbnVtYmVycyBpcyAy
NiDDlyA2MyDDlyA3OCDDlyAxNCA9IDE3ODg2OTYuDQoNCldoYXQgaXMgdGhlIGdy
ZWF0ZXN0IHByb2R1Y3Qgb2YgZm91ciBhZGphY2VudCBudW1iZXJzIGluIHRoZSBz
YW1lIGRpcmVjdGlvbiAodXAsIGRvd24sIGxlZnQsIHJpZ2h0LCBvciBkaWFnb25h
bGx5KSBpbiB0aGUgMjDDlzIwIGdyaWQ/
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

void LineProduct(int (* pdata)[20])
{
  int i,j;
  int product;
  int MaxProduct = 0;

  for(i = 0; i < 20; i++)
  {
    for(j = 0; j < 17; j++)
    {
      product = pdata[i][j] * pdata[i][j + 1] * pdata[i][j + 2] * pdata[i][j + 3];

      if(MaxProduct < product)
      {
        MaxProduct = product;
      }
    }
  }

  //printf("Max LineProduct     %d\n",MaxProduct);
}

void ColumnProduct(int (* pdata)[20])
{
  int i,j;
  int product;
  int MaxProduct = 0;

  for(i = 0; i < 17; i++)
  {
    for(j = 0; j < 20; j++)
    {
      product = pdata[i][j] * pdata[i + 1][j] * pdata[i + 2][j] * pdata[i + 3][j];

      if(MaxProduct < product)
      {
        MaxProduct = product;
      }
    }
  }

  //printf("Max ColumnProduct   %d\n",MaxProduct);
}

void DownDiagProduct(int (* pdata)[20])
{
  int i,j;
  int product;
  int MaxProduct = 0;

  for(i = 0; i < 17; i++)
  {
    for(j = 0; j < 17; j++)
    {
      product = pdata[i][j] * pdata[i + 1][j + 1] * pdata[i + 2][j + 2] * pdata[i + 3][j + 3];

      if(MaxProduct < product)
      {
        MaxProduct = product;
      }
    }
  }

  //printf("Max DownDiagProduct %d\n",MaxProduct);
}

void UpDiagProduct(int (* pdata)[20])
{
  int i,j;
  int product;
  int MaxProduct = 0;

  for(i = 3; i < 20; i++)
  {
    for(j = 0; j < 17; j++)
    {
      product = pdata[i][j] * pdata[i - 1][j + 1] * pdata[i - 2][j + 2] * pdata[i - 3][j + 3];

      if(MaxProduct < product)
      {
        MaxProduct = product;
      }
    }
  }

  printf("%d\n",MaxProduct);
}

int main()
{
  // clock_t start = clock();

  int data[20][20] = {{ 8,  2, 22, 97, 38, 15,  0, 40,  0, 75,  4,  5,  7, 78, 52, 12, 50, 77, 91,  8},
                      {49, 49, 99, 40, 17, 81, 18, 57, 60, 87, 17, 40, 98, 43, 69, 48,  4, 56, 62, 00},
                      {81, 49, 31, 73, 55, 79, 14, 29, 93, 71, 40, 67, 53, 88, 30, 03, 49, 13, 36, 65},
                      {52, 70, 95, 23,  4, 60, 11, 42, 69, 24, 68, 56,  1, 32, 56, 71, 37,  2, 36, 91},
                      {22, 31, 16, 71, 51, 67, 63, 89, 41, 92, 36, 54, 22, 40, 40, 28, 66, 33, 13, 80},
                      {24, 47, 32, 60, 99,  3, 45,  2, 44, 75, 33, 53, 78, 36, 84, 20, 35, 17, 12, 50},
                      {32, 98, 81, 28, 64, 23, 67, 10, 26, 38, 40, 67, 59, 54, 70, 66, 18, 38, 64, 70},
                      {67, 26, 20, 68,  2, 62, 12, 20, 95, 63, 94, 39, 63,  8, 40, 91, 66, 49, 94, 21},
                      {24, 55, 58,  5, 66, 73, 99, 26, 97, 17, 78, 78, 96, 83, 14, 88, 34, 89, 63, 72},
                      {21, 36, 23,  9, 75,  0, 76, 44, 20, 45, 35, 14,  0, 61, 33, 97, 34, 31, 33, 95},
                      {78, 17, 53, 28, 22, 75, 31, 67, 15, 94,  3, 80,  4, 62, 16, 14,  9, 53, 56, 92},
                      {16, 39,  5, 42, 96, 35, 31, 47, 55, 58, 88, 24,  0, 17, 54, 24, 36, 29, 85, 57},
                      {86, 56,  0, 48, 35, 71, 89,  7,  5, 44, 44, 37, 44, 60, 21, 58, 51, 54, 17, 58},
                      {19, 80, 81, 68,  5, 94, 47, 69, 28, 73, 92, 13, 86, 52, 17, 77,  4, 89, 55, 40},
                      { 4, 52,  8, 83, 97, 35, 99, 16,  7, 97, 57, 32, 16, 26, 26, 79, 33, 27, 98, 66},
                      {88, 36, 68, 87, 57, 62, 20, 72,  3, 46, 33, 67, 46, 55, 12, 32, 63, 93, 53, 69},
                      { 4, 42, 16, 73, 38, 25, 39, 11, 24, 94, 72, 18,  8, 46, 29, 32, 40, 62, 76, 36},
                      {20, 69, 36, 41, 72, 30, 23, 88, 34, 62, 99, 69, 82, 67, 59, 85, 74,  4, 36, 16},
                      {20, 73, 35, 29, 78, 31, 90,  1, 74, 31, 49, 71, 48, 86, 81, 16, 23, 57,  5, 54},
                      { 1, 70, 54, 71, 83, 51, 54, 69, 16, 92, 33, 48, 61, 43, 52,  1, 89, 19, 67, 48}};

  LineProduct(&data[0]);
  ColumnProduct(&data[0]);
  DownDiagProduct(&data[0]);
  UpDiagProduct(&data[0]);

  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}
