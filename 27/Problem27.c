/*
RXVsZXIgcHVibGlzaGVkIHRoZSByZW1hcmthYmxlIHF1YWRyYXRpYyBmb3JtdWxh
Og0KDQpuwrIgKyBuICsgNDENCg0KSXQgdHVybnMgb3V0IHRoYXQgdGhlIGZvcm11
bGEgd2lsbCBwcm9kdWNlIDQwIHByaW1lcyBmb3IgdGhlIGNvbnNlY3V0aXZlIHZh
bHVlcyBuID0gMCB0byAzOS4NCkhvd2V2ZXIsIHdoZW4gbiA9IDQwLCA0MF4oMikg
KyA0MCArIDQxID0gNDAoNDAgKyAxKSArIDQxIGlzIGRpdmlzaWJsZSBieSA0MSwN
CmFuZCBjZXJ0YWlubHkgd2hlbiBuID0gNDEsIDQxwrIgKyA0MSArIDQxIGlzIGNs
ZWFybHkgZGl2aXNpYmxlIGJ5IDQxLg0KDQpVc2luZyBjb21wdXRlcnMsIHRoZSBp
bmNyZWRpYmxlIGZvcm11bGEgIG7CsiAtIDc5biArIDE2MDEgd2FzIGRpc2NvdmVy
ZWQsDQp3aGljaCBwcm9kdWNlcyA4MCBwcmltZXMgZm9yIHRoZSBjb25zZWN1dGl2
ZSB2YWx1ZXMgbiA9IDAgdG8gNzkuDQpUaGUgcHJvZHVjdCBvZiB0aGUgY29lZmZp
Y2llbnRzLCAtNzkgYW5kIDE2MDEsIGlzIC0xMjY0NzkuDQoNCkNvbnNpZGVyaW5n
IHF1YWRyYXRpY3Mgb2YgdGhlIGZvcm06DQoNCiAgICBuwrIgKyBhbiArIGIsIHdo
ZXJlIHxhfCA8IDEwMDAgYW5kIHxifCA8IDEwMDANCg0KICAgIHdoZXJlIHxufCBp
cyB0aGUgbW9kdWx1cy9hYnNvbHV0ZSB2YWx1ZSBvZiBuDQogICAgZS5nLiB8MTF8
ID0gMTEgYW5kIHwtNHwgPSA0DQoNCkZpbmQgdGhlIHByb2R1Y3Qgb2YgdGhlIGNv
ZWZmaWNpZW50cywgYSBhbmQgYiwgZm9yIHRoZSBxdWFkcmF0aWMgZXhwcmVzc2lv
biB0aGF0IHByb2R1Y2VzIHRoZQ0KbWF4aW11bSBudW1iZXIgb2YgcHJpbWVzIGZv
ciBjb25zZWN1dGl2ZSB2YWx1ZXMgb2Ygbiwgc3RhcnRpbmcgd2l0aCBuID0gMC4=
*/

#include <stdint.h>
#include "../EulerLib/euler.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

uint32_t EulerFormula(uint32_t a, uint32_t b, uint32_t n)
{
  return ((n * n) + (a * n) + b);
}

uint32_t nResult(uint32_t a, uint32_t b)
{
  uint32_t n = 0;

  while (n < 1000)
  {
    if(isPrime(EulerFormula(a,b,n)) == 0)
    {
      return (n);
    }

    n++;
  }

  return (0);
}


uint32_t Result(int32_t *max_a, int32_t *max_b)
{
  int32_t b = 0;
  int32_t a = -999;
  uint32_t n = 0;

  uint32_t max_n = 0;

  while (a < 1000)
  {
    b = -999;

    while (b < 1000)
    {
      if(isPrime(abs(b)))
      {
        n = nResult(a,b);

        if(n > max_n)
        {
          *max_b = b;
          *max_a = a;
          max_n = n;

          //printf("n² + %dn + %d = prime for 0 to %u\n", (int)a, (int)b, (unsigned)n);
        }
      }

      b++;
    }

    a += 2;
  }

  return (max_n);
}

int main()
{

  // clock_t start = clock();

  int32_t b = 0;
  int32_t a = 0;
//  uint32_t n = 0;

  //n = Result(&a,&b);
  Result(&a,&b);

  //printf("\nn² + %dn + %d = prime for 0 to %u and coefficient product %d\n", (int)a, (int)b, (unsigned)n, (int)(a *b));
  printf("%d\n", (int)(a *b));

  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}
