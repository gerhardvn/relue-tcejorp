#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

char * LoadName(FILE *namefile)
{
  char buf[256];
  char ch;
  unsigned int i = 0;
  int ReadName = 0;
  char * s = NULL;

  do
  {
    ch = fgetc(namefile);
    buf[i] = ch;

    if((ch >= 'A') && (ch <= 'Z'))
    {
      i++;
      ReadName = 1;
    }
    else if(ReadName)
    {
      ch = EOF;
    }

  } while (ch != EOF);

  if(i)
  {
    buf[i] = 0;
    s = malloc(i);
    strcpy(s,buf);
  }

  return s;

}

void swap(char * strings[10000], int a, int b)
{
    char *temp;

    temp=strings[a];
    strings[a]=strings[b];
    strings[b]=temp;
}

int partition(char * strings[10000], int left, int right)
{
  int first,pivot;

  first = left;
  pivot = right--;
  while(left<=right)
  {
    while(strcmp(strings[left],strings[pivot]) < 0)
    {
      left++;
    }
    
    while( (right>=first) && (strcmp(strings[right],strings[pivot])>=0) )
    {
      right--;
    }
    
    if(left<right)
    {
      swap(strings, left,right);
      left++;
    }
  }

  if(left!=pivot)
  {
    swap(strings, left,pivot);
  }

  return left;
}


void QuickSort(char * strings[10000], int left, int right)
{
    int p;

    if(left>=right)
        return;
    p = partition(strings, left, right);

    QuickSort(strings, left,p-1);
    QuickSort(strings, p+1,right);
}

int main()
{
  // clock_t start = clock();

  FILE *numfile;
  char * names[10000];

  unsigned i = 0;
  unsigned j;

  unsigned answer = 0;
  unsigned score = 0;
  unsigned count = 0;
  unsigned length = 0;

  numfile = fopen("names.txt", "r");

  if (numfile != NULL)
  {

    do
    {
      names[i] = LoadName(numfile);
      i++;
    }while (names[i - 1] != NULL);

    fclose(numfile);

    QuickSort(names,0,(i-2));

    while(count < (i-1))
    {
      length = strlen(names[count]);

      score = 0;
      for (j = 0; j < length; j++)
      {
        score += (((names[count])[j]) - ('A' - 1));
      }

      count++;
      answer += (score * count);
      //printf("%4u: %20s : %8u\n", count, names[count-1], score);
      
      if((count % 20) == 0)
      {
        fflush(stdout); 
      }
      free(names[count-1]);
    }

 
    printf("%u\n",answer);
  }

  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);
  return 0;
}
