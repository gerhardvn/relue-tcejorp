/*
VGhlIHNlcmllcywgMTEgKyAyMiArIDMzICsgLi4uICsgMTAxMCA9IDEwNDA1MDcx
MzE3Lg0KRmluZCB0aGUgbGFzdCB0ZW4gZGlnaXRzIG9mIHRoZSBzZXJpZXMsIDEx
ICsgMjIgKyAzMyArIC4uLiArIDEwMDAxMDAwLg0K
*/

#include <gmp.h>
#include <stdio.h>
#include <string.h>

int main()
{
  mpz_t answer;
  mpz_t temp;
  mpz_t power;

  unsigned int n = 1;

  mpz_init(answer);
  mpz_init(temp);
  mpz_init(power);

  mpz_set_ui(answer,0);

  while (n <= 1000)
  {
    mpz_set(temp, answer);
    mpz_ui_pow_ui(power,n,n);
    mpz_add(answer, power, temp);
    n++;
  }

  //gmp_printf("%Zd", answer);
  char buffy[8192];
  gmp_sprintf(buffy,"%Zd", answer);
  printf("%s\n",&buffy[strlen(buffy) -10]);

  mpz_clear(answer);
  mpz_clear(power);
  mpz_clear(temp);

  return 0;
}
