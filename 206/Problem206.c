/*
Q29uY2VhbGVkIFNxdWFyZQ0KUHJvYmxlbSAyMDYNCkZpbmQgdGhlIHVuaXF1ZSBw
b3NpdGl2ZSBpbnRlZ2VyIHdob3NlIHNxdWFyZSBoYXMgdGhlIGZvcm0gMV8yXzNf
NF81XzZfN184XzlfMCwNCndoZXJlIGVhY2gg4oCcX+KAnSBpcyBhIHNpbmdsZSBk
aWdpdC4=
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <inttypes.h>

int32_t is_answer(int64_t n)
{
	int32_t i;
  
	if (n%10 != 0)
	{
    return 0;
  }

	for (i = 9; i > 0; --i)
	{
		n /= 100; 
		if (n%10 != i)
    {
			return 0;
    }
	}

	return 1;
}

int main()
{
  // clock_t start = clock();

  int64_t n = 1010101010;

  while(!is_answer(n*n))
  {
    n +=10;
  }
  
  printf("%"PRIu64"\n",n);

  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);
  return 0;
}