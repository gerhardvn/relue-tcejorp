/*
VGhlIHNlcXVlbmNlIG9mIHRyaWFuZ2xlIG51bWJlcnMgaXMgZ2VuZXJhdGVkIGJ5
IGFkZGluZyB0aGUgbmF0dXJhbCBudW1iZXJzLiBTbyB0aGUgN3RoIHRyaWFuZ2xl
IG51bWJlciB3b3VsZCBiZSAxICsgMiArIDMgKyA0ICsgNSArIDYgKyA3ID0gMjgu
IFRoZSBmaXJzdCB0ZW4gdGVybXMgd291bGQgYmU6DQoNCjEsIDMsIDYsIDEwLCAx
NSwgMjEsIDI4LCAzNiwgNDUsIDU1LCAuLi4NCg0KTGV0IHVzIGxpc3QgdGhlIGZh
Y3RvcnMgb2YgdGhlIGZpcnN0IHNldmVuIHRyaWFuZ2xlIG51bWJlcnM6DQoNCiAx
OiAxDQogMzogMSwzDQogNjogMSwyLDMsNg0KMTA6IDEsMiw1LDEwDQoxNTogMSwz
LDUsMTUNCjIxOiAxLDMsNywyMQ0KMjg6IDEsMiw0LDcsMTQsMjgNCldlIGNhbiBz
ZWUgdGhhdCAyOCBpcyB0aGUgZmlyc3QgdHJpYW5nbGUgbnVtYmVyIHRvIGhhdmUg
b3ZlciBmaXZlIGRpdmlzb3JzLg0KDQpXaGF0IGlzIHRoZSB2YWx1ZSBvZiB0aGUg
Zmlyc3QgdHJpYW5nbGUgbnVtYmVyIHRvIGhhdmUgb3ZlciBmaXZlIGh1bmRyZWQg
ZGl2aXNvcnM/
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "../EulerLib/euler.h"

unsigned int NextTriangularNumber(void)
{
  static unsigned int  n = 0;
  static unsigned int  sum = 0;

  n++;
  sum += n;
  return sum;
}


int main()
{
  // clock_t start = clock();
  unsigned int i = 0;
  unsigned int f;

  while(f < 500)
  {
    i = NextTriangularNumber();
    f = FactorCount(i);
  }

  printf("%u\n",f);

  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}
