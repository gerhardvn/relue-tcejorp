/*
QSB1bml0IGZyYWN0aW9uIGNvbnRhaW5zIDEgaW4gdGhlIG51bWVyYXRvci4gVGhl
IGRlY2ltYWwgcmVwcmVzZW50YXRpb24gb2YgdGhlDQp1bml0IGZyYWN0aW9ucyB3
aXRoIGRlbm9taW5hdG9ycyAyIHRvIDEwIGFyZSBnaXZlbjoNCg0KICAgIF4oMSkv
XygyKSA9ICAgMC41DQogICAgXigxKS9fKDMpID0gICAwLigzKQ0KICAgIF4oMSkv
Xyg0KSA9ICAgMC4yNQ0KICAgIF4oMSkvXyg1KSA9ICAgMC4yDQogICAgXigxKS9f
KDYpID0gICAwLjEoNikNCiAgICBeKDEpL18oNykgPSAgIDAuKDE0Mjg1NykNCiAg
ICBeKDEpL18oOCkgPSAgIDAuMTI1DQogICAgXigxKS9fKDkpID0gICAwLigxKQ0K
ICAgIF4oMSkvXygxMCkgID0gICAwLjENCg0KV2hlcmUgMC4xKDYpIG1lYW5zIDAu
MTY2NjY2Li4uLCBhbmQgaGFzIGEgMS1kaWdpdCByZWN1cnJpbmcgY3ljbGUuIEl0
IGNhbiBiZQ0Kc2VlbiB0aGF0IF4oMSkvXyg3KSBoYXMgYSA2LWRpZ2l0IHJlY3Vy
cmluZyBjeWNsZS4NCg0KRmluZCB0aGUgdmFsdWUgb2YgZCA8IDEwMDAgZm9yIHdo
aWNoIF4oMSkvXyhkKSBjb250YWlucyB0aGUgbG9uZ2VzdCByZWN1cnJpbmcNCmN5
Y2xlIGluIGl0cyBkZWNpbWFsIGZyYWN0aW9uIHBhcnQu
*/

#include <stdio.h>
#include <inttypes.h>
#include <string.h>

int main()
{
  unsigned i;
  unsigned sequence = 0;
  unsigned num;

  for (i = 999; i > 1; i--)
  {
 
    if (sequence >= i) {
      break;
    }

    unsigned foundRemainders[i];
    memset(foundRemainders,0,sizeof(unsigned)*i);
    
    unsigned value = 1;
    unsigned position = 0;
 
    while ((foundRemainders[value] == 0) && (value != 0))
    {
      foundRemainders[value] = position;
      value *= 10;
      value %= i;
      position++;
    }
 
    if ((position - foundRemainders[value]) > sequence)
    {
      sequence = position - foundRemainders[value];
      num = i;
    }
    
  }

  //printf("%u, %u\n", num, sequence);
  printf("%u\n", num);
  
  return 0;
}