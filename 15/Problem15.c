/*
U3RhcnRpbmcgaW4gdGhlIHRvcCBsZWZ0IGNvcm5lciBvZiBhIDLDlzIgZ3JpZCwg
dGhlcmUgYXJlIDYgcm91dGVzICh3aXRob3V0IGJhY2t0cmFja2luZykgdG8gdGhl
IGJvdHRvbSByaWdodCBjb3JuZXIuDQpIb3cgbWFueSByb3V0ZXMgYXJlIHRoZXJl
IHRocm91Z2ggYSAyMMOXMjAgZ3JpZD8NCg==
*/

#include <gmp.h>
#include <stdio.h>

int main()
{
  mpz_t fac4;
  mpz_t fac2;
  mpz_t answer;
  mpz_t temp;
  mpz_init(answer);
  mpz_init(fac4);
  mpz_init(fac2);
  mpz_init(temp);

  mpz_fac_ui(fac4, 40);
  mpz_fac_ui(fac2, 20);

  mpz_tdiv_q(temp, fac4, fac2);
  mpz_tdiv_q(answer, temp, fac2);

  gmp_printf("%Zd\n" , answer);
  
  return 0;
}

