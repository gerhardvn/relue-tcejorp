/*
A common security method used for online banking is to ask the user for three random characters from a passcode. For example, if the passcode was 531278, they may ask for the 2nd, 3rd, and 5th characters; the expected reply would be: 317.

The text file, https://projecteuler.net/project/resources/p079_keylog.txt, contains fifty successful login attempts.

Given that the three characters are always asked for in order, analyse the file so as to determine the shortest possible secret passcode of unknown length.
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

int in_collection[3][10] = {0};
int scale_collection[7][10] = {0};

void print_collection(int collection[][10], int dim)
{
  for (int n = 0; n < 10; n++)
  {
    printf ("%2d ", (n + 1) %10);
    for (int i = 0; i < dim; i++)
    {
      printf("%4d ", collection[i][n]);
    }
    printf("\n");
  }
}

void upscale_collection(void)
{
  memcpy((void *)scale_collection[0], (void *)in_collection[0], sizeof(int) * 10);

  for (int n = 0; n < 10; n++)
  {
    scale_collection[1][n] = in_collection[0][n] + in_collection[1][n];
  }

  memcpy((void *)scale_collection[6], (void *)in_collection[2], sizeof(int) * 10);
}
  
int main()
{
  // clock_t start = clock();

  FILE *numfile;
  char login[16];

  numfile = fopen("p079_keylog.txt", "r");

  if (numfile != NULL)
  {

    while (fgets(login, 128, numfile) != NULL)
    {
      for (int i = 0; i < 3; i++)
      {
        int d = login[i] - '1';
        in_collection[i][d]++;
      }
    }
    fclose(numfile);

  }
  print_collection(in_collection, 3);
  upscale_collection();  
  print_collection(scale_collection,7);
  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);
  return 0;
}
