/*
A common security method used for online banking is to ask the user for three random characters from a passcode. For example, if the passcode was 531278, they may ask for the 2nd, 3rd, and 5th characters; the expected reply would be: 317.

The text file, https://projecteuler.net/project/resources/p079_keylog.txt, contains fifty successful login attempts.

Given that the three characters are always asked for in order, analyse the file so as to determine the shortest possible secret passcode of unknown length.
*/
#include "../EulerLib/euler.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>


int in_collection[3][10] = {0};
int keys[100];
int max_key = 0;

void print_collection(int collection[][10], int dim)
{
  for (int n = 0; n < 10; n++)
  {
    printf ("%2d ", (n + 1) %10);
    for (int i = 0; i < dim; i++)
    {
      printf("%4d ", collection[i][n]);
    }
    printf("\n");
  }
}

int find_lead_digit(int collection[][10])
{
  int max_count = 0;
  int max_digit = 0;
  
  for (int i = 0; i < 10; i++)
  {
    if (max_count < collection[0][i])
    {
      max_digit = i;
      max_count = collection[0][i];
    }
  }
  //printf("Max digit: %d\n",max_digit+1);
  return (max_digit + 1);
}

void find_missing_digits(int collection[][10])
{
  for (int i = 0; i < 10; i++)
  {
    if ((0 == collection[0][i]) &&
        (0 == collection[1][i]) &&
        (0 == collection[2][i]))
    {
      printf("missing digit: %d\n",i +1);
    }
  }
}

int all_key_solution(int key, int keys[], int max_key)
{
  int works = 0;
  
  // Should come from missing digits.
  if((ContainsDigit(key,4) != 21) &&
     (ContainsDigit(key,5) != 21))
  {
    return works;
  }
 
  for (int i = 0; i < max_key; i++)
  {
    int xkey = key;
    
    for(int j = 3; j > 0; j--)
    {
      int x = GetDigit(keys[i],j);
      int p = ContainsDigit(xkey, x);
      if(p == 21) // Not found
      {
        return works; // good matches before failure.
      }
      RemoveDigit(xkey, p);
    }
    works++;
  }
  
  return works;
}

int main()
{
  clock_t start = clock();

  FILE *numfile;
  char login[16];

  numfile = fopen("p079_keylog.txt", "r");

  // Read all the data into memory
  if (numfile != NULL)
  {

    while (fgets(login, 128, numfile) != NULL)
    {
      keys[max_key] = atoi(login); 
      max_key++; // Count of keys read.
      
      for (int i = 0; i < 3; i++)
      {
        // Count occurrence of key in location
        int d = login[i] - '1';
        in_collection[i][d]++;
      }
    }
    fclose(numfile);

  }
  
  // Never found in key log
  find_missing_digits(in_collection);

  // First key most likely digit.
  int lead_digit = find_lead_digit(in_collection);
  int keydigits =  100;
  int solution_count = 0;
  int key = 0;
  
  do
  {
    
    keydigits *= 10;
    key = lead_digit * keydigits;
    int end = (lead_digit  + 1) * keydigits;
    if(key % keydigits != 0) // Searched through all keys
    {
      break;
    }
    
    printf("Search Key Range %d - %d\n", key, end);
    
    do
    {
      solution_count = all_key_solution(key, keys, max_key);
      if (solution_count == max_key)
      {
        break;
      }
      key++;
    }while(key < end);
    
  }while((solution_count < max_key));

  if((key % keydigits != 0) && (key > 0))
  {
    printf("Key: %d", key);
  }

  printf("\nTime elapsed: %f", ((double)clock() - start) / CLOCKS_PER_SEC);
  return 0;
}
