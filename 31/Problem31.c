/*
SW4gdGhlIFVuaXRlZCBLaW5nZG9tIHRoZSBjdXJyZW5jeSBpcyBtYWRlIHVwIG9m
IHBvdW5kICjCoykgYW5kIHBlbmNlIChwKS4gVGhlcmUgYXJlIGVpZ2h0IGNvaW5z
IGluIGdlbmVyYWwgY2lyY3VsYXRpb246DQoNCjFwLCAycCwgNXAsIDEwcCwgMjBw
LCA1MHAsIMKjMSAoMTAwcCksIGFuZCDCozIgKDIwMHApLg0KSXQgaXMgcG9zc2li
bGUgdG8gbWFrZSDCozIgaW4gdGhlIGZvbGxvd2luZyB3YXk6DQoNCjHDl8KjMSAr
IDHDlzUwcCArIDLDlzIwcCArIDHDlzVwICsgMcOXMnAgKyAzw5cxcA0KSG93IG1h
bnkgZGlmZmVyZW50IHdheXMgY2FuIMKjMiBiZSBtYWRlIHVzaW5nIGFueSBudW1i
ZXIgb2YgY29pbnM/
*/

#include "../EulerLib/euler.h"
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <math.h>

/* qsort int comparison function */ 
int int_cmp(const void *a, const void *b) 
{ 
    const unsigned *ia = (const unsigned *)a; // casting pointer types 
    const unsigned *ib = (const unsigned *)b;
    return *ib - *ia; 
	/* integer comparison: returns negative if b > a 
	and positive if a > b */ 
} 

int valueinarray(unsigned val, unsigned arr[], unsigned size)
{
  for(unsigned i = 0; i < size; i++)
  {
    if(arr[i] == val)
    {
      return 1;
    }
  }
  return 0;
}

unsigned filter(unsigned *factors, unsigned count)
{
  unsigned newcount = 0;
  unsigned valid[] = {1, 2, 5, 10, 20, 50, 100, 200, 500, 1000};

  factors[count] = 200;
  count++;

  for (unsigned i = 0; i < count; i++)
  {
     if(valueinarray(factors[i],valid, 10))
     {
       factors[newcount] = factors[i];
       newcount++;
     }
  }
  
  return (newcount);
}

unsigned recurser (unsigned val, unsigned current, unsigned arr[], unsigned size, unsigned level)
{
  unsigned ways = 0;
  
  //printf( "\n%u %u %u %u %u\n", val, current, arr[level], size, level);
  
  for (unsigned i = current; i <= val; i += arr[level])
  {
    if (i == val)
    {
      ways += 1;
    }
    else
    {
      if(level < (size-1))
      {
       ways += recurser (val, i, arr, size, level + 1);
      }
    }
  }
  
  return ways;
}

int main()
{
  unsigned target = 200;
  uint32_t factors[target];
  unsigned factorcount = Factorise(target, factors);
  
  factorcount = filter(factors,factorcount);
  qsort(factors, factorcount, sizeof(unsigned), int_cmp);

  unsigned ways = recurser(target, 0, factors, factorcount, 0);
  
  printf("%u\n",ways);
  
  return 0;
}

