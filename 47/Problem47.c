/*
VGhlIGZpcnN0IHR3byBjb25zZWN1dGl2ZSBudW1iZXJzIHRvIGhhdmUgdHdvIGRp
c3RpbmN0IHByaW1lIGZhY3RvcnMgYXJlOg0KDQoxNCA9IDIgw5cgNw0KMTUgPSAz
IMOXIDUNCg0KVGhlIGZpcnN0IHRocmVlIGNvbnNlY3V0aXZlIG51bWJlcnMgdG8g
aGF2ZSB0aHJlZSBkaXN0aW5jdCBwcmltZSBmYWN0b3JzIGFyZToNCg0KNjQ0ID0g
MsKyIMOXIDcgw5cgMjMNCjY0NSA9IDMgw5cgNSDDlyA0Mw0KNjQ2ID0gMiDDlyAx
NyDDlyAxOS4NCg0KRmluZCB0aGUgZmlyc3QgZm91ciBjb25zZWN1dGl2ZSBpbnRl
Z2VycyB0byBoYXZlIGZvdXIgZGlzdGluY3QgcHJpbWUgZmFjdG9ycyBlYWNoLiBX
aGF0IGlzIHRoZSBmaXJzdCBvZiB0aGVzZSBudW1iZXJzPw==
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "../EulerLib/euler.h"

#define CONSECUTIVE 4

int main()
{
  unsigned int i = 1000;
  unsigned int factors[32];
  int f;
  int exit = 0;

  while (exit < CONSECUTIVE)
  {
    i++;
    f = PrimeFactorise(i, &factors[0]);

    if(f == CONSECUTIVE)
    {
      exit++;
    }
    else
    {
      exit = 0;
    }

  }

  printf("%u \n",(i - CONSECUTIVE + 1));

  return 0;
}
