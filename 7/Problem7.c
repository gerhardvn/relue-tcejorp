/*
QnkgbGlzdGluZyB0aGUgZmlyc3Qgc2l4IHByaW1lIG51bWJlcnM6IDIsIDMsIDUs
IDcsIDExLCBhbmQgMTMsIHdlIGNhbiBzZWUgdGhhdCB0aGUgNnRoIHByaW1lIGlz
IDEzLg0KV2hhdCBpcyB0aGUgMTAgMDAxc3QgcHJpbWUgbnVtYmVyPyANCg==
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <inttypes.h>
#include "../EulerLib/euler.h"

int main()
{
  // clock_t start = clock();
  unsigned int i = 2;
  unsigned int n =0;

  while (n < 10001)
  {
    if(isPrime((uint64_t)i))
    {
      n++;
      //printf("%u: %u\n",n, i);
    }
    i++;
  }

  i--;
  printf("%u\n",i);

  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}