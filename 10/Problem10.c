/*
VGhlIHN1bSBvZiB0aGUgcHJpbWVzIGJlbG93IDEwIGlzIDIgKyAzICsgNSArIDcg
PSAxNy4NCkZpbmQgdGhlIHN1bSBvZiBhbGwgdGhlIHByaW1lcyBiZWxvdyB0d28g
bWlsbGlvbi4gDQo=
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <inttypes.h>
#include "../EulerLib/euler.h"

int main()
{
  // clock_t start = clock();
  uint64_t i = 3;
  uint64_t sum = 2;

  while (i < 2000000)
  {
    if(isPrime(i))
    {
      sum += i;
    }
    i += 2;
  }

  //printf("%" PRIu64 ": %" PRIu64 "\n",i,sum);
  printf("%" PRIu64 "\n",sum);
  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}