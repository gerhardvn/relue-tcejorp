/* 
VGhlIG51bWJlciwgMTk3LCBpcyBjYWxsZWQgYSBjaXJjdWxhciBwcmltZSBiZWNh
dXNlIGFsbCByb3RhdGlvbnMgb2YgdGhlIGRpZ2l0czogMTk3LCA5NzEsIGFuZCA3
MTksIGFyZSB0aGVtc2VsdmVzIHByaW1lLg0KDQpUaGVyZSBhcmUgdGhpcnRlZW4g
c3VjaCBwcmltZXMgYmVsb3cgMTAwOiAyLCAzLCA1LCA3LCAxMSwgMTMsIDE3LCAz
MSwgMzcsIDcxLCA3MywgNzksIGFuZCA5Ny4NCg0KSG93IG1hbnkgY2lyY3VsYXIg
cHJpbWVzIGFyZSB0aGVyZSBiZWxvdyBvbmUgbWlsbGlvbj8=
*/

#include "../EulerLib/euler.h"
#include <stdio.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int isCirclePrime(unsigned long long int n)
{
  int i;
  int digits = HowManyDigits(n);
  unsigned long long int r;

  if(digits == 1)
  {
    return 1;
  }

  for(i = 1; i < digits; i++)
  {
    r = RotateNumber(n,i);
    if (isPrime(r) == 0)
    {
      return 0;
    }
  }

  return 1;
}

int main()
{
  // clock_t start = clock();
  unsigned long long int i = 3;
  unsigned long long int sum = 1;

  while (i < 1000000)
  {
    if(isPrime(i))
    {
      if(isCirclePrime(i) != 0)
      {
        sum ++;
        // printf("%llu / %llu \n",sum, i);
      }
    }
    i += 2;
  }

  printf("%llu\n",sum);
  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

  return 0;
}
