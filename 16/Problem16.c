/*
MjE1ID0gMzI3NjggYW5kIHRoZSBzdW0gb2YgaXRzIGRpZ2l0cyBpcyAzICsgMiAr
IDcgKyA2ICsgOCA9IDI2Lg0KV2hhdCBpcyB0aGUgc3VtIG9mIHRoZSBkaWdpdHMg
b2YgdGhlIG51bWJlciAyMTAwMD8NCg==
*/

#include <gmp.h>
#include <stdio.h>

unsigned HowManyDigits(mpz_t n)
{
  mpz_t i,r;
  mpz_init_set_ui (i,1); 
  mpz_init(r);
  
  unsigned digits = 0;

  do
  {
    mpz_mul_ui(r,i,10);
    mpz_set(i,r); 
    digits++;
    mpz_fdiv_q(r,n,i);
  }while (mpz_cmp_ui (r,0) !=0);

  mpz_clear (i);
  mpz_clear (r);
  
  return digits;
} 

unsigned GetDigit(mpz_t n, unsigned digit)
{
  mpz_t ret,i, powwow;
  mpz_init(ret);
  mpz_init(i);

  mpz_init(powwow);
  mpz_ui_pow_ui(powwow,10,digit);  

  mpz_mod(i, n, powwow);
  mpz_ui_pow_ui(powwow,10,digit - 1); 
  mpz_fdiv_q(ret,i,powwow);

  mpz_clear (i);
  mpz_clear (powwow);

  return mpz_get_ui(ret);
} 

int main()
{

  mpz_t answer;
  mpz_init(answer);

  mpz_t pow_ans;
  mpz_init(pow_ans);
  
  mpz_ui_pow_ui (pow_ans, 2, 1000);
  //gmp_printf("%Zd\n" , pow_ans);

  unsigned digits = HowManyDigits(pow_ans);
  //printf("%u\n", digits);
  
  unsigned sum = 0;
  unsigned j;
  for (j = 1; j <= digits; j++)
  {
    sum += (GetDigit(pow_ans,j));
    //printf("%u", (GetDigit(pow_ans,j)));
  }
  printf("%u\n", sum);
  
  return 0;
}

