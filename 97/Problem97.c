#include <gmp.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
  mpz_t answer;
  mpz_t product;
  mpz_t power;

  mpz_init(answer);
  mpz_init(product);
  mpz_init(power);

  mpz_ui_pow_ui(power,2ul,7830457ul);
  mpz_mul_ui (product, power, 28433ul);
  mpz_add_ui(answer, product, 1ul);

  //gmp_printf("%Zd", answer);

  char * buffy = (char *)malloc(4194304);
  gmp_sprintf(buffy,"%Zd", answer);
  printf("%s\n",&buffy[strlen(buffy) -10]);
  free((void *)buffy);
  
  mpz_clear(answer);
  mpz_clear(power);
  mpz_clear(product);

  return 0;
}