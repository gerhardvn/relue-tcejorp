// I just reused code I have created before to solve this problem for fun.
// https://gitlab.com/gerhardvn/relue-tcejorp/-/tree/master/14
// This code is reasonably fast for most numbers.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int collatz(long start_point)
{
  clock_t start = clock();
  
  unsigned int i = (unsigned int)start_point;
  unsigned int steps;
  unsigned int n;
  unsigned int MaxSteps = 0;
  unsigned int MaxStepsI = 0;

  while(i)
  {
    n = i;
    steps = 1;

    while(n != 1)
    {
      //printf("%u -> ",n);
      if((n % 2) == 0)
      {
        n /= 2;
      }
      else
      {
        n = (3*n) + 1;
      }
      steps++;
    }

    if(steps > MaxSteps)
    {
      MaxSteps = steps;
      MaxStepsI = i;
    }

    i--;
  }

  printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);
  return MaxStepsI;
}

int main( int argc, char *argv[] )
{
  char *ptr;
  long start_point;
  unsigned int answer;
   
  if( argc == 2 )
  {
    start_point = strtol( argv[1], &ptr, 10);
    answer = collatz( start_point );
    printf("%u\n",answer);
  }
  else if( argc > 2 )
  {
    printf("Too many arguments supplied.\n");
  }
  else
  {
    printf("One argument expected.\n");
  }
}