/*
VGhlIGZvbGxvd2luZyBpdGVyYXRpdmUgc2VxdWVuY2UgaXMgZGVmaW5lZCBmb3Ig
dGhlIHNldCBvZiBwb3NpdGl2ZSBpbnRlZ2VyczoNCg0KbiA/IG4vMiAobiBpcyBl
dmVuKQ0KbiA/IDNuICsgMSAobiBpcyBvZGQpDQoNClVzaW5nIHRoZSBydWxlIGFi
b3ZlIGFuZCBzdGFydGluZyB3aXRoIDEzLCB3ZSBnZW5lcmF0ZSB0aGUgZm9sbG93
aW5nIHNlcXVlbmNlOg0KMTMgPyA0MCA/IDIwID8gMTAgPyA1ID8gMTYgPyA4ID8g
NCA/IDIgPyAxDQoNCkl0IGNhbiBiZSBzZWVuIHRoYXQgdGhpcyBzZXF1ZW5jZSAo
c3RhcnRpbmcgYXQgMTMgYW5kIGZpbmlzaGluZyBhdCAxKSBjb250YWlucyAxMCB0
ZXJtcy4gQWx0aG91Z2ggaXQgaGFzIG5vdCBiZWVuIHByb3ZlZCB5ZXQgKENvbGxh
dHogUHJvYmxlbSksIGl0IGlzIHRob3VnaHQgdGhhdCBhbGwgc3RhcnRpbmcgbnVt
YmVycyBmaW5pc2ggYXQgMS4NCg0KV2hpY2ggc3RhcnRpbmcgbnVtYmVyLCB1bmRl
ciBvbmUgbWlsbGlvbiwgcHJvZHVjZXMgdGhlIGxvbmdlc3QgY2hhaW4/DQoNCk5P
VEU6IE9uY2UgdGhlIGNoYWluIHN0YXJ0cyB0aGUgdGVybXMgYXJlIGFsbG93ZWQg
dG8gZ28gYWJvdmUgb25lIG1pbGxpb24u
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int main()
{
  // clock_t start = clock();
  unsigned int i = 1000000;
  unsigned int steps;
  unsigned int n;
  unsigned int MaxSteps = 0;
  unsigned int MaxStepsI = 0;

  while(i)
  {
    n = i;
    steps = 1;

    while(n != 1)
    {
      //printf("%u -> ",n);
      if((n % 2) == 0)
      {
        n /= 2;
      }
      else
      {
        n = (3*n) + 1;
      }
      steps++;
    }

    if(steps > MaxSteps)
    {
      MaxSteps = steps;
      MaxStepsI = i;
    }

    i--;
  }

  //printf("%u %u\n",MaxStepsI, MaxSteps);
  printf("%u\n",MaxStepsI);

  // printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);
  return 0;
}
