// To optimize speed for large numbers this solution cache previous computed answers.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int collatz(long start_point)
{
  clock_t start = clock();
  
  long sequence;
  int MaxSteps = 0;
  int MaxStepsI = 0;

  // Cache for previous answers
  int * cache = (int *) malloc((start_point + 1) * sizeof(int));
  for (int j = 0; j < (start_point + 1); j++)
  {
    cache[j] = -1; 
  }
  cache[1] = 1;

  for (int i = 2; i <= start_point; i++)
  {
    sequence = i;
    int k = 0;
    
    while (sequence != 1 && sequence >= i)
    {
      k++;
      if ((sequence % 2) == 0)
      {
        sequence = sequence / 2;
      }
      else
      {
        sequence = sequence * 3 + 1;
      }
    }

    // Cache answer
    cache[i] = k + cache[sequence];
 
    //Check if answer is in cache
    if (cache[i] > MaxSteps)
    {
      MaxSteps = cache[i];
      MaxStepsI = i;
    }
  }
  
  free (cache);
  // printf("%u %u\n",MaxStepsI, MaxSteps);
  // printf("%u\n",MaxStepsI);

  printf("\nTime elapsed: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);
  
  return MaxStepsI;
}

int main( int argc, char *argv[] )
{
  char *ptr;
  long start_point;
  unsigned int answer;
   
  if( argc == 2 )
  {
    start_point = strtol( argv[1], &ptr, 10);
    answer = collatz( start_point );
    printf("%u\n",answer);
  }
  else if( argc > 2 )
  {
    printf("Too many arguments supplied.\n");
  }
  else
  {
    printf("One argument expected.\n");
  }
}